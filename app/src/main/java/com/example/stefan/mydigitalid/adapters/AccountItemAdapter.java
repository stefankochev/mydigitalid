package com.example.stefan.mydigitalid.adapters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorDescription;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.List;

/**
 * Created by Stefan on 1/21/2018.
 */

public class AccountItemAdapter extends RecyclerView.Adapter<AccountItemAdapter.AccountViewHolder> implements INameableAdapter{

    private List<Account> list;
    private Context context;
    private LayoutInflater layoutInflater;
    private PackageManager packageManager;

    public class AccountViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener

    {

        public TextView accName;
        public TextView accType;
        public ImageView imageView;

        public AccountViewHolder(View itemView) {
            super(itemView);
            accName = (TextView)itemView.findViewById(R.id.accName);
            accType = (TextView)itemView.findViewById(R.id.accType);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

    }

    public AccountItemAdapter(List<Account> list, Context context){
        this.list = list;
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        packageManager = context.getPackageManager();
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.account_row,parent,false);
        return new AccountViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountViewHolder holder, int position) {
        final Account acc = list.get(position);
        Drawable icon = getIconForAccount(acc,AccountManager.get(context));
        holder.imageView.setImageDrawable(icon);
        holder.accName.setText(acc.name);
        holder.accType.setText(acc.type);
    }

    private Drawable getIconForAccount(Account account, AccountManager manager) {
        AuthenticatorDescription[] descriptions =  manager.getAuthenticatorTypes();
        for (AuthenticatorDescription description: descriptions) {
            if (description.type.equals(account.type)) {
                return packageManager.getDrawable(description.packageName, description.iconId, null);
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        return list.get(element).name.charAt(0);
    }

}
