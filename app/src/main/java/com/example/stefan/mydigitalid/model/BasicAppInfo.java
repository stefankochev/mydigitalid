package com.example.stefan.mydigitalid.model;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.io.Serializable;

/**
 * Created by Stefan on 2/13/2018.
 */

public class BasicAppInfo implements Serializable {

    public String appName;
    public int iconRes;
    public String packageName;

    public BasicAppInfo(ResolveInfo resolveInfo, PackageManager packageManager){
        this.appName = resolveInfo.loadLabel(packageManager).toString();
        this.iconRes = resolveInfo.getIconResource();
        this.packageName = resolveInfo.activityInfo.packageName;
    }


    @Override
    public boolean equals(Object obj) {
        BasicAppInfo info = (BasicAppInfo)obj;
        return appName.equals(info.appName);
    }
}
