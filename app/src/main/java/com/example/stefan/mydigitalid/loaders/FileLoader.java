package com.example.stefan.mydigitalid.loaders;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.example.stefan.mydigitalid.model.FileStats;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by kristijan on 1/20/18.
 */

public class FileLoader extends AsyncTaskLoader<List<FileStats>> {

    List<FileStats> stats;

    public FileLoader(Context context) {
        super(context);
        stats = new ArrayList<>();
        stats.add(new FileStats("image", 0, 0));
        stats.add(new FileStats("text", 0, 0));
        stats.add(new FileStats("video", 0, 0));
        stats.add(new FileStats("audio", 0, 0));

    }

    @Override
    public List<FileStats> loadInBackground() {
//        List<File> files = getListFiles(Environment.getExternalStorageDirectory());
//
//        files.addAll(getListFiles(getSecondaryStorageDirectory()));
        List<File> files = new ArrayList<>();
        for(File dir : getAllStorageLocations()){
            files.addAll(getListFiles(dir));
        }

        eraseData();

        for (File file : files) {
            updateStats(file);
        }

        writeToDb();

        return stats;
    }

    private List<File> getAllStorageLocations() {
        List<File> storageLocations = new ArrayList<>();
        File storageDir = Environment.getExternalStorageDirectory();
        String storagePath = storageDir.getAbsolutePath();
        File[] extFileDirs = getContext().getExternalFilesDirs(null);
        String suffixToStrip = null;
        for (File nextFile : extFileDirs) {
            String nextPath = nextFile.getAbsolutePath();
            if (nextPath.startsWith(storagePath)) {
                suffixToStrip = nextPath.substring(storagePath.length());
                break;
            }
        }
        if (suffixToStrip != null) {
            for (File nextFile : extFileDirs) {
                String nextPath = nextFile.getAbsolutePath();
                int stripIndex = nextPath.lastIndexOf(suffixToStrip);
                if (stripIndex > -1) {
                    File path = new File(nextPath.substring(0, stripIndex));
                    storageLocations.add(path);
                }
            }
        }
        if (storageLocations.size() == 0) {
            storageLocations.add(storageDir);
        }
        return storageLocations;
    }


    @Nullable
    private File getSecondaryStorageDirectory() {
        String strSDCardPath = System.getenv("SECONDARY_STORAGE");

        if ((strSDCardPath == null) || (strSDCardPath.length() == 0)) {
            strSDCardPath = System.getenv("EXTERNAL_SDCARD_STORAGE");
        }

//      If may get a full path that is not the right one, even if we don't have the SD Card there.
//      We just need the "/mnt/extSdCard/" i.e and check if it's writable
        if (strSDCardPath != null) {
            if (strSDCardPath.contains(":")) {
                strSDCardPath = strSDCardPath.substring(0, strSDCardPath.indexOf(":"));
            }
            File externalFilePath = new File(strSDCardPath);

            if (externalFilePath.exists()) {
                return externalFilePath;
            }
        }
        return null;
    }

    private void writeToDb() {
        SharedPreferences sharedPref = getContext()
                .getSharedPreferences("com.example.stefan.mydigitalid.PREFERENCES_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String uniqueID;


        uniqueID = sharedPref.getString("uniqueUserID", null);

        if (uniqueID == null) {
            uniqueID = UUID.randomUUID().toString();
            editor.putString("uniqueUserID", uniqueID);
            editor.apply();
        }

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userNode = database
                .getReference("filesData")
                .child(uniqueID);

        for (FileStats stat : this.stats) {
            //write in Firebase
            userNode.child(stat.type).child("numberOfFiles").setValue(stat.numberOfFiles);
            userNode.child(stat.type).child("totalSize").setValue(stat.totalSize);

            //write in Shared Preferences
            editor.putFloat(stat.type, stat.totalSize);
        }
        editor.apply();
    }

    private void eraseData() {
        String[] niza = {"image", "video", "audio", "text"};
        for (String s : niza) {
            FileStats fileStats = getStatsByType(s);
            fileStats.numberOfFiles = 0;
            fileStats.totalSize = 0;
        }
    }

    private FileStats getStatsByType(String type) {
        for (FileStats stat : stats) {
            if (stat.type.equals(type))
                return stat;
        }
        return new FileStats("", 0, 0);
    }

    private void updateStats(File file) {
        Uri uri = Uri.fromFile(file);
        String type = getMimeType(uri);

        FileStats stat = null;

        if (type.contains("image")) {
            stat = getStatsByType("image");
        } else if (type.contains("video")) {
            stat = getStatsByType("video");
        } else if (type.contains("text") || file.getName().toLowerCase().endsWith(".pdf")) {
            stat = getStatsByType("text");
        } else if (type.contains("audio")) {
            stat = getStatsByType("audio");
        }

        if (stat != null) {
            stat.numberOfFiles++;
            stat.totalSize += file.length();
        }
    }

    private String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContext().getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString().toLowerCase());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }

        if (mimeType == null)
            return "no type found";

        return mimeType;
    }

    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        try {
            File[] files = parentDir.listFiles();
            for (File file : files) {
                try {
                    if (file.isDirectory()) {
                        inFiles.addAll(getListFiles(file));
                        //inFiles.add(file);
                    } else {
                        inFiles.add(file);
                    }
                } catch (Exception e) {
                    Log.d("LOAD ERROR", e.getMessage());
                }

            }

            Collections.sort(inFiles);
        } catch (Exception e) {
            Log.d("LOAD ERROR 1", e.getMessage());
        }

        return inFiles;
    }

}
