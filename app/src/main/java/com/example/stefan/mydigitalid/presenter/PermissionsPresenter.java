package com.example.stefan.mydigitalid.presenter;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.example.stefan.mydigitalid.loaders.PermissionsLoader;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.view.PermissionsSectionFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 2/8/2018.
 */

public class PermissionsPresenter implements Serializable, LoaderManager.LoaderCallbacks<List<PermissionGroup>> {

    protected PermissionsSectionFragment mView;

    public void attachView(PermissionsSectionFragment view) {
        mView = view;
    }

    public void onRefresh(){
        mView.showProgressBar();
        mView.getLoaderManager().initLoader(3,null,this).forceLoad();
    }

    public void onDestroy(){
        this.mView = null;
    }


    @Override
    public Loader<List<PermissionGroup>> onCreateLoader(int id, Bundle args) {
        return new PermissionsLoader(mView.getContext().getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<List<PermissionGroup>> loader, List<PermissionGroup> data) {
        mView.hideProgressBar();
        mView.setGroupList(data);
    }

    @Override
    public void onLoaderReset(Loader<List<PermissionGroup>> loader) {
        if(mView!=null){
            mView.setGroupList(new ArrayList<PermissionGroup>());
        }
    }
}
