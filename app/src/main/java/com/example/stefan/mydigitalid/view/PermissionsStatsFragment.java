package com.example.stefan.mydigitalid.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.FileAverageStats;
import com.example.stefan.mydigitalid.model.PermissionsStats;
import com.example.stefan.mydigitalid.retrofit.PermissionsApiClient;
import com.example.stefan.mydigitalid.retrofit.PermissionsApiInterface;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Stefan on 2/17/2018.
 */

public class PermissionsStatsFragment extends Fragment {


    private TextView camera;
    private TextView location;
    private TextView microphone;
    public ImageView noConnection;
    AnimatedVectorDrawable avd;
    public RelativeLayout content;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConnectionBroadcastReceiver connectionBroadcastReceiver;
    private BarChart barChart;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionBroadcastReceiver = new ConnectionBroadcastReceiver();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_permissions_stats,container,false);

        barChart = view.findViewById(R.id.PermissionsStatisticsBarChart);

        camera = (TextView)view.findViewById(R.id.cam_stat);
        location = (TextView)view.findViewById(R.id.loc_stat);
        microphone = (TextView)view.findViewById(R.id.mic_stat);
        content = (RelativeLayout)view.findViewById(R.id.content_perm_stats);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBarStats);
        progressBar.setVisibility(View.GONE);

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshStats);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
                boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
                if(connected){
                    checkConnection();
                }
                else{
                    Toast.makeText(getContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        final ViewStub stub = view.findViewById(R.id.stub_no_connection);
        avd = (AnimatedVectorDrawable) getContext().getDrawable(R.drawable.avd_no_connection);

        noConnection = (ImageView) stub.inflate();
        noConnection.setVisibility(View.GONE);

        checkConnection();

        //getPermissionsStats();

        return view;
    }

    public boolean checkConnection(){
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if(!connected) {
            if (noConnection != null && avd != null) {
                noConnection.setVisibility(View.VISIBLE);
                content.setVisibility(View.GONE);
                noConnection.setImageDrawable(avd);
                avd.start();
            }
        }
        else{
            showProgressBar();
            noConnection.setVisibility(View.GONE);
            getPermissionsStats();
            //content.setVisibility(View.VISIBLE);
        }
        swipeRefreshLayout.setRefreshing(false);
        return connected;
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        getContext().registerReceiver(connectionBroadcastReceiver, filter);
        //checkConnection();
        super.onResume();
    }

    @Override
    public void onPause() {
        getContext().unregisterReceiver(connectionBroadcastReceiver);
        super.onPause();
    }

    public void getPermissionsStats(){

        PermissionsApiInterface apiService =
                PermissionsApiClient.getClient().create(PermissionsApiInterface.class);

        Call<PermissionsStats> call = apiService.getStats();

        call.enqueue(new Callback<PermissionsStats>() {
            @Override
            public void onResponse(Call<PermissionsStats> call, Response<PermissionsStats> response) {
                PermissionsStats stats = response.body();
                if(stats!=null){
                    setUI(stats);
                }
                hideProgressBar();
                noConnection.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<PermissionsStats> call, Throwable t) {
                Log.d("RETROFIT_FAIL","FAIL");
            }
        });

    }

    public void showProgressBar(){progressBar.setVisibility(View.VISIBLE);}
    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

    public void setUI(PermissionsStats stats){
        SharedPreferences sharedPref = getContext().getSharedPreferences("com.example.stefan.mydigitalid.PREFERENCES_FILE", Context.MODE_PRIVATE);
        float cam = sharedPref.getFloat("cam",-1);
        float loc = sharedPref.getFloat("loc",-1);
        float mic = sharedPref.getFloat("mic",-1);

        if(cam > stats.camera + 1){
            camera.setText("NOT SAFE");
        }
        else if (cam < stats.camera - 1){
            camera.setText("SAFE");
        }
        else {
            camera.setText("MEDIUM SAFETY");
        }

        if(loc > stats.location + 1){
            location.setText("NOT SAFE");
        }
        else if(loc < stats.location -1){
            location.setText("SAFE");
        }
        else{
            location.setText("MEDIUM SAFETY");
        }

        if(mic > stats.microphone + 1){
            microphone.setText("NOT SAFE");
        }
        else if(mic < stats.microphone - 1){
            microphone.setText("SAFE");
        }
        else {
            microphone.setText("MEDIUM SAFETY");
        }
        fillChart(stats,cam,loc,mic);
    }


    private void fillChart(PermissionsStats stats,float cam, float loc, float mic) {

        List<BarEntry> myEntries = new ArrayList<>();
        List<BarEntry> averageEntries = new ArrayList<>();

        myEntries.add(new BarEntry(0, cam));
        myEntries.add(new BarEntry(1, loc));
        myEntries.add(new BarEntry(2, mic));


        averageEntries.add(new BarEntry(0, stats.camera));
        averageEntries.add(new BarEntry(1, stats.location));
        averageEntries.add(new BarEntry(2, stats.microphone));

        BarDataSet myDataSet = new BarDataSet(myEntries, "My premissions grants");
        BarDataSet averageDataSet = new BarDataSet(averageEntries, "Average permissions grants");

        myDataSet.setColor(Color.parseColor("#7CB342")); // colorPrimaryLight
        averageDataSet.setColor(Color.parseColor("#33691E")); // colorPrimaryDark

        PermissionsStatsFragment.MyValueFormatter valueFormatter = new PermissionsStatsFragment.MyValueFormatter();
        myDataSet.setValueFormatter(valueFormatter);
        averageDataSet.setValueFormatter(valueFormatter);

        float groupSpace = 0.2f;
        float barSpace = 0.1f; // x2 dataset
        float barWidth = 0.8f; // x2 dataset

        String[] labels = {"Camera", "Locations", "Microphone"};

        BarData data = new BarData(myDataSet, averageDataSet);
        data.setBarWidth(barWidth); // set the width of each bar
        barChart.setData(data);
        barChart.groupBars(-0.0f, groupSpace, barSpace); // perform the "explicit" grouping
        barChart.setFitBars(false);
        barChart.setHorizontalScrollBarEnabled(true);
        barChart.setDrawGridBackground(false);
        barChart.setPinchZoom(false);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setClickable(false);
        barChart.getData().setHighlightEnabled(false);
        Description description = new Description();
        description.setEnabled(false);
        barChart.setDescription(description);
        Legend legend = barChart.getLegend();


        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new PermissionsStatsFragment.LabelFormatter(labels));
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(6);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis left = barChart.getAxisLeft();
        left.setDrawZeroLine(false);
        left.setDrawLabels(false);
        left.setDrawGridLines(false);
        left.setAxisMinimum(0f);

        YAxis right = barChart.getAxisRight();
        right.setDrawZeroLine(false);
        right.setDrawLabels(false);
        right.setDrawGridLines(false);
        right.setAxisMinimum(0f);

        barChart.invalidate(); // refresh
    }

    private class LabelFormatter implements IAxisValueFormatter {
        private final String[] mLabels;

        public LabelFormatter(String[] labels) {
            mLabels = labels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            int val = (int) value;
            if (val % 2 == 1 && (val - 1) / 2 < mLabels.length) {
                return mLabels[(val - 1) / 2]; //odd number to array index
            } else return "";
        }
    }

    public class MyValueFormatter implements IValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return String.format(Locale.US,"%.1f", value);
        }
    }

    public class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            if(connected){
                checkConnection();
            }
        }
    }

}
