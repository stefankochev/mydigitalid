package com.example.stefan.mydigitalid.adapters;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.AppInfo;
import com.example.stefan.mydigitalid.presenter.AppSectionPermissionsPresenter;
import com.example.stefan.mydigitalid.view.AppSectionPermissionsFragment;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;


/**
 * Created by Stefan on 1/16/2018.
 */

public class AppItemAdapter extends RecyclerView.Adapter<AppItemAdapter.AppViewHolder> implements INameableAdapter {

    private List<AppInfo> list;
    private Context context;
    private FragmentActivity fragmentActivity;

    private static final int UNSELECTED = -1;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;

    public class AppViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private ExpandableLayout expandableLayout;
        public TextView textView, appSizeView;
        public ImageView imageView;
        public RelativeLayout expandButton;
        public ImageButton arrow;
        private Button btnLaunch;
        private Button btnPermissions;

        public AppViewHolder(View itemView) {
            super(itemView);
            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            expandableLayout.setInterpolator(new OvershootInterpolator());
            expandableLayout.setOnExpansionUpdateListener(this);
            expandButton = itemView.findViewById(R.id.expand_button);
            expandButton.setOnClickListener(this);
            imageView = (ImageView)itemView.findViewById(R.id.imageView);
            textView = (TextView)itemView.findViewById(R.id.activityName);
            arrow = (ImageButton) itemView.findViewById(R.id.arrow);
            btnLaunch = (Button) itemView.findViewById(R.id.btn_launch);
            appSizeView = (TextView)itemView.findViewById(R.id.appSize);
            btnPermissions = (Button)itemView.findViewById(R.id.btn_permission);
        }


        @Override
        public void onClick(View v) {
            AppViewHolder holder = (AppViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
            if (holder != null) {
                holder.expandButton.setSelected(false);
                holder.expandableLayout.collapse();
                holder.arrow.animate().rotation(0f);
                this.setIsRecyclable(true);
            }

            int position = getAdapterPosition();
            if (position == selectedItem) {
                selectedItem = UNSELECTED;
            } else {
                expandButton.setSelected(true);
                expandableLayout.expand();
                selectedItem = position;
                arrow.animate().rotation(90f);
                this.setIsRecyclable(false);
            }
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            if (state == ExpandableLayout.State.EXPANDING) {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

    }

    public AppItemAdapter(List<AppInfo> list, Context context, RecyclerView recyclerView, FragmentActivity activity){
        this.list = list;
        this.context = context;
        this.recyclerView = recyclerView;
        this.fragmentActivity = activity;
    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.application_row,parent,false);
        return new AppViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppViewHolder holder, int position) {
        final AppInfo info = list.get(position);

        boolean isSelected = position == selectedItem;
        holder.expandButton.setSelected(isSelected);
        holder.expandableLayout.setExpanded(isSelected, false);

        holder.textView.setText(info.label);
        holder.imageView.setImageDrawable(info.icon);
        holder.appSizeView.setText(info.size);



        if(holder.expandableLayout.isExpanded()){
            holder.arrow.setRotation(90f);
        }
        else{
            holder.arrow.setRotation(0f);
        }
        holder.btnLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + info.packageName));
                context.startActivity(intent);
            }
        });

        holder.btnPermissions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
                AppSectionPermissionsPresenter presenter = new AppSectionPermissionsPresenter();
                AppSectionPermissionsFragment permissionsFragment = AppSectionPermissionsFragment.newInstance(presenter,info.packageName,holder.textView.getText().toString());
                fragmentTransaction.setCustomAnimations(R.anim.slide_from_right,R.anim.exit_to_right,R.anim.slide_from_right,R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.base,permissionsFragment);
                fragmentTransaction.addToBackStack("Permissions fragment");
                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        AppInfo info = list.get(element);
        return  info.label.charAt(0);
    }

}
