package com.example.stefan.mydigitalid.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.CallsItemAdapter;
import com.example.stefan.mydigitalid.adapters.FavouriteContactsAdapter;
import com.example.stefan.mydigitalid.adapters.MostAverageTimeTalkedAdapter;
import com.example.stefan.mydigitalid.adapters.MostTimeTalkedAdapter;
import com.example.stefan.mydigitalid.model.Call;
import com.example.stefan.mydigitalid.model.CallStatistic;
import com.example.stefan.mydigitalid.presenter.CallsSectionPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jovan on 05-Feb-18.
 */

public class CallsSectionFragment extends Fragment
{
    public static final String CALLS_ARRAY="com.example.stefan.mydigitalid.calls.array";
    public static final String BROADCAST_INTENT_ACTION="com.example.stefan.mydigitalid.calls.broadcast.intent.action";
    private static final String ARG_CALLS_PRESENTER="com.example.stefan.mydigitalid.calls.presenter";
    private CallsSectionPresenter mPresenter;
    private List<Call> mCallsList;
    private RecyclerView mRecyclerView1;
    private RecyclerView mRecyclerView2;
    private RecyclerView mRecyclerView3;
    private CallsItemAdapter mCallsItemAdapter;
    private CallsBroadcastReceiver mCallsBroadcastReceiver;
    private TabLayout mCallsTabLayout;
    private TextView mFavouritesContactsText;
    private TextView mMostTimeTalkedText;
    private TextView mMostAverageTimeTalkedWithText;
    //private Button mFavouritesButton;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args=getArguments();
        if(args!=null)
            mPresenter=(CallsSectionPresenter)args.getSerializable(ARG_CALLS_PRESENTER);
        mCallsList=new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_section_calls,container,false);
        mRecyclerView1=view.findViewById(R.id.calls_recycler_view_1);
        mCallsItemAdapter=new CallsItemAdapter(mCallsList,getLayoutInflater(),getContext());
        mRecyclerView1.setAdapter(mCallsItemAdapter);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView2=view.findViewById(R.id.calls_recycler_view_2);
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView3=view.findViewById(R.id.calls_recycler_view_3);
        mRecyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        mFavouritesContactsText=view.findViewById(R.id.calls_favourites_label);
        mMostTimeTalkedText=view.findViewById(R.id.calls_most_time_talked_label);
        mMostAverageTimeTalkedWithText=view.findViewById(R.id.calls_most_average_time_talked_label);
        mCallsTabLayout=view.findViewById(R.id.calls_tab);
        mCallsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                int position=tab.getPosition();
                if(position==0)
                {
                    mCallsItemAdapter=new CallsItemAdapter(mCallsList,getLayoutInflater(),getContext());
                    mRecyclerView1.setAdapter(mCallsItemAdapter);
                    mFavouritesContactsText.setText("");
                    mFavouritesContactsText.setPadding(0,0,0,0);
                    mMostTimeTalkedText.setText("");
                    mMostTimeTalkedText.setPadding(0,0,0,0);
                    mRecyclerView2.setAdapter(null);
                    mMostAverageTimeTalkedWithText.setText("");
                    mMostAverageTimeTalkedWithText.setPadding(0,0,0,0);
                    mRecyclerView3.setAdapter(null);
                }
                else
                {
                    int length=0;
                    ArrayList<CallStatistic> callStatistics=mCallsItemAdapter.getMostFrequentlyCalled();
                    if(callStatistics.size()<3)
                        length=callStatistics.size();
                    else
                        length=3;
                    FavouriteContactsAdapter favouriteContactsAdapter=new FavouriteContactsAdapter(callStatistics.subList(0,length),getLayoutInflater());
                    mRecyclerView1.setAdapter(favouriteContactsAdapter);
                    mFavouritesContactsText.setText("Favourites");
                    mFavouritesContactsText.setPadding(0,25,0,0);
                    mFavouritesContactsText.setTextColor(Color.RED);
                    mFavouritesContactsText.setTextSize(20);
                    callStatistics=mCallsItemAdapter.getMostTimeTalked();
                    if(callStatistics.size()<3)
                        length=callStatistics.size();
                    else
                        length=3;
                    MostTimeTalkedAdapter mostTimeTalkedAdapter=new MostTimeTalkedAdapter(callStatistics.subList(0,length),getLayoutInflater());
                    mRecyclerView2.setAdapter(mostTimeTalkedAdapter);
                    mMostTimeTalkedText.setText("Most Time Talked With");
                    mMostTimeTalkedText.setPadding(0,25,0,0);
                    mMostTimeTalkedText.setTextColor(Color.RED);
                    mMostTimeTalkedText.setTextSize(20);
                    callStatistics=mCallsItemAdapter.getMostAveragetTimeTalked();
                    if(callStatistics.size()<3)
                        length=callStatistics.size();
                    else
                        length=3;
                    MostAverageTimeTalkedAdapter mostAverageTimeTalkedAdapter=new MostAverageTimeTalkedAdapter(callStatistics.subList(0,length),getLayoutInflater());
                    mRecyclerView3.setAdapter(mostAverageTimeTalkedAdapter);
                    mMostAverageTimeTalkedWithText.setText("Most Average Time Talked With");
                    mMostAverageTimeTalkedWithText.setPadding(0,25,0,0);
                    mMostAverageTimeTalkedWithText.setTextColor(Color.RED);
                    mMostAverageTimeTalkedWithText.setTextSize(20);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //mFavouritesButton=view.findViewById(R.id.favourites);
        /*mFavouritesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                FavouriteContactsAdapter adapter=new FavouriteContactsAdapter(mCallsItemAdapter.getMostFrequentlyCalled().subList(0,10),getLayoutInflater());
                mRecyclerView.setAdapter(adapter);
            }
        });*/
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter=new IntentFilter(BROADCAST_INTENT_ACTION);
        mCallsBroadcastReceiver=new CallsBroadcastReceiver();
        getContext().registerReceiver(mCallsBroadcastReceiver,intentFilter);
        if(mPresenter!=null)
        {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getContext().unregisterReceiver(mCallsBroadcastReceiver);
        if(mPresenter!=null)
        {
            mPresenter.onDestroy();
        }
    }

    public void setCallsList(List<Call>callsList)
    {
        mCallsList.clear();
        mCallsList.addAll(callsList);
        mCallsItemAdapter.notifyDataSetChanged();
    }
    public static CallsSectionFragment newInstance(CallsSectionPresenter presenter)
    {
        Bundle args=new Bundle();
        args.putSerializable(ARG_CALLS_PRESENTER,presenter);
        CallsSectionFragment fragment=new CallsSectionFragment();
        fragment.setArguments(args);
        return fragment;
    }
    private class CallsBroadcastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            setCallsList((ArrayList<Call>)intent.getSerializableExtra(CALLS_ARRAY));
            mCallsItemAdapter.getCallsStatistics();
        }
    }
}
