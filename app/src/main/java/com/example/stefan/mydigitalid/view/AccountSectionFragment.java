package com.example.stefan.mydigitalid.view;


import android.accounts.Account;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.AccountItemAdapter;
import com.example.stefan.mydigitalid.presenter.AccountSectionPresenter;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by Stefan on 1/21/2018.
 */

public class AccountSectionFragment extends Fragment{

    private static final String ARG_ACCOUNTS_PRESENTER = "accounts.presenter";

    private AccountSectionPresenter mPresenter;

    private List<Account> accountsList = new ArrayList<>();
    private AccountItemAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if(arguments != null) {
            mPresenter = (AccountSectionPresenter) arguments.getSerializable(ARG_ACCOUNTS_PRESENTER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_accounts,container,false);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerViewAcc);
        mAdapter = new AccountItemAdapter(accountsList,getActivity().getApplicationContext());

        TouchScrollBar touchScrollBar = (TouchScrollBar)view.findViewById(R.id.touchScrollBar);
        touchScrollBar.setIndicator(new AlphabetIndicator(getActivity().getApplicationContext()),false);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        if(mPresenter!=null) {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }

        return view;
    }



    @Override
    public void onResume() {
        mPresenter.attachView(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        mPresenter.onDestroy();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        mPresenter.onDestroy();
        super.onSaveInstanceState(outState);
    }

    public void setAccountsList(Account[] accounts){
        accountsList.clear();
        accountsList.addAll(Arrays.asList(accounts));
        mAdapter.notifyDataSetChanged();
    }

    public static AccountSectionFragment newInstance(AccountSectionPresenter presenter){
        Bundle args = new Bundle();
        args.putSerializable(ARG_ACCOUNTS_PRESENTER, presenter);

        AccountSectionFragment fragment = new AccountSectionFragment();
        fragment.setArguments(args);

        return fragment;
    }

}
