package com.example.stefan.mydigitalid.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;

import com.example.stefan.mydigitalid.model.Call;
import com.example.stefan.mydigitalid.view.CallsSectionFragment;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CallsIntentService extends IntentService {

    public CallsIntentService() {
        super("CallsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {

        @SuppressLint("MissingPermission")
        Cursor data = getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, null, null, CallLog.Calls.DATE + " DESC");
        if(data!=null)
        {
            ArrayList<Call> callsList=new ArrayList<>();
            int numberColumn=data.getColumnIndex(CallLog.Calls.NUMBER);
            int callerNameColumn=data.getColumnIndex(CallLog.Calls.CACHED_NAME);
            int typeColumn=data.getColumnIndex(CallLog.Calls.TYPE);
            int durationColumn=data.getColumnIndex(CallLog.Calls.DURATION);
            int dateColumn=data.getColumnIndex(CallLog.Calls.DATE);
            while(data.moveToNext())
            {
                Call c=new Call();
                c.number=data.getString(numberColumn);
                c.callerName=data.getString(callerNameColumn);
                c.type=data.getString(typeColumn);
                c.duration=data.getString(durationColumn);
                Date dayAndTime=new Date(Long.valueOf(data.getString(dateColumn)));
                c.date= DateFormat.getDateTimeInstance().format(dayAndTime).toString();
                callsList.add(c);
            }
            Intent i=new Intent(CallsSectionFragment.BROADCAST_INTENT_ACTION);
            //ogranicvam broj na elementi vo lista sho ja prajcam vo broadcast za da uspejt da se pratit ako imat mn povici
            int callsListLength=0;
            if(callsList.size()>1500)
                callsListLength=1500;
            else
                callsListLength=callsList.size();
            List<Call> calls=callsList.subList(0,callsListLength);
            //pretvoram vo ArrayList zsh tie se serializable a samo takvi elementi mozet da odet vo broadcast
            ArrayList<Call> finalCallsList=new ArrayList<>();
            finalCallsList.addAll(calls);
            i.putExtra(CallsSectionFragment.CALLS_ARRAY,finalCallsList);
            sendBroadcast(i);
            data.close();

        }
    }
}
