package com.example.stefan.mydigitalid.adapters;


import android.content.pm.PermissionInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.PermissionGroup;

import java.util.List;

/**
 * Created by Stefan on 2/5/2018.
 */

public class AppPermissionsAdapter extends RecyclerView.Adapter<AppPermissionsAdapter.AppPermissionsHolder>{

    private List<PermissionGroup> groups;

    public class AppPermissionsHolder extends RecyclerView.ViewHolder{

        public ImageView groupIcon;
        public TextView groupName;
        public TextView groupStatus;

        public AppPermissionsHolder(View itemView) {
            super(itemView);
            groupIcon = (ImageView)itemView.findViewById(R.id.group_icon);
            groupName = (TextView)itemView.findViewById(R.id.group_name);
            groupStatus = (TextView)itemView.findViewById(R.id.group_status);
        }
    }

    public AppPermissionsAdapter(List<PermissionGroup> groups){
        this.groups = groups;
    }

    @Override
    public AppPermissionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.app_permissions_row,parent,false);
        return new AppPermissionsHolder(view);
    }

    @Override
    public void onBindViewHolder(AppPermissionsHolder holder, int position) {

        PermissionGroup group = groups.get(position);

        holder.groupIcon.setImageResource(group.iconRes);

        holder.groupName.setText(group.labelRes);

        if(group.isGranted()){
            holder.groupStatus.setText("Granted");
        }
        else{
            holder.groupStatus.setText("Not Granted");
        }
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

}
