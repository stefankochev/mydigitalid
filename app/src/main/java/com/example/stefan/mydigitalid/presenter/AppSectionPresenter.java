package com.example.stefan.mydigitalid.presenter;


import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.example.stefan.mydigitalid.loaders.AppsLoader;
import com.example.stefan.mydigitalid.model.AppInfo;
import com.example.stefan.mydigitalid.view.AppSectionFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 1/16/2018.
 */

public class AppSectionPresenter implements Serializable, LoaderManager.LoaderCallbacks<List<AppInfo>> {

    protected AppSectionFragment mView;

    public void attachView(AppSectionFragment view) {
        mView = view;

    }

    public void onDestroy(){
        this.mView = null;
    }

    public void onRefresh(){
        mView.showProgressBar();
        mView.getLoaderManager().initLoader(1,null,this).forceLoad();
    }

    public void reload(){
        mView.getLoaderManager()
                .initLoader(1,null, this)
                .forceLoad();
    }

    @Override
    public Loader<List<AppInfo>> onCreateLoader(int id, Bundle args) {
        return new AppsLoader(mView.getActivity().getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<List<AppInfo>> loader, List<AppInfo> data) {
        mView.setAppsList(data);
        mView.hideProgressBar();
    }

    @Override
    public void onLoaderReset(Loader<List<AppInfo>> loader) {
        if(mView!=null){
            mView.setAppsList(new ArrayList<AppInfo>());
        }
    }
}
