package com.example.stefan.mydigitalid.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.stefan.mydigitalid.model.BasicAppInfo;
import com.example.stefan.mydigitalid.model.PermissionApps;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.view.PermissionAppsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionAppsPagerAdapter extends FragmentPagerAdapter {

    private PermissionGroup permissionGroup;

    public PermissionAppsPagerAdapter(FragmentManager fm, PermissionGroup permissionGroup) {
        super(fm);
        this.permissionGroup = permissionGroup;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){

            PermissionApps apps = new PermissionApps(permissionGroup.appsGranted);

            //PermissionAppsPresenter presenter = new PermissionAppsPresenter(apps);
            Bundle args = new Bundle();
            args.putSerializable("permission_apps",apps);
            PermissionAppsFragment fragment = new PermissionAppsFragment();
            fragment.setArguments(args);


            return fragment;
        }
        else if(position == 1){
            List<BasicAppInfo> appsInfo = new ArrayList<>();
            appsInfo.addAll(permissionGroup.allApps);
            appsInfo.removeAll(permissionGroup.appsGranted);
            PermissionApps apps = new PermissionApps(appsInfo);;

            Bundle args = new Bundle();
            args.putSerializable("permission_apps",apps);
            PermissionAppsFragment fragment = new PermissionAppsFragment();
            fragment.setArguments(args);
            return fragment;
        }
        else{
            PermissionApps apps = new PermissionApps(permissionGroup.allApps);

            Bundle args = new Bundle();
            args.putSerializable("permission_apps",apps);
            PermissionAppsFragment fragment = new PermissionAppsFragment();
            fragment.setArguments(args);

            return fragment;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String[] titles = {"Allowed","Not Allowed","All"};
        return titles[position];
    }
}
