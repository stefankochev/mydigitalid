package com.example.stefan.mydigitalid.loaders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.content.AsyncTaskLoader;

import com.example.stefan.mydigitalid.model.AppInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Stefan on 1/31/2018.
 */

public class AppsLoader extends AsyncTaskLoader<List<AppInfo>> {


    public AppsLoader(Context context){
        super(context);
    }

    @Override
    public List<AppInfo> loadInBackground() {
        Intent query = new Intent(Intent.ACTION_MAIN);
        query.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> list = getContext().getPackageManager().queryIntentActivities(query, PackageManager.MATCH_ALL);
        Collections.sort(list, new Comparator<ResolveInfo>() {
            @Override
            public int compare(ResolveInfo r1, ResolveInfo r2) {
                return r1.loadLabel(getContext().getPackageManager()).toString()
                        .compareToIgnoreCase(r2.loadLabel(getContext().getPackageManager()).toString());
            }
        });

        List<AppInfo> appInfoList = new ArrayList<>();
        for(ResolveInfo info : list){
            AppInfo appInfo = new AppInfo(info,getContext().getPackageManager());
            appInfoList.add(appInfo);
        }
        return appInfoList;
    }
}
