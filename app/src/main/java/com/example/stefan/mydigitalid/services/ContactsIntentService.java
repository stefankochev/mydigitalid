package com.example.stefan.mydigitalid.services;
import android.app.IntentService;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import com.example.stefan.mydigitalid.model.Contact;
import com.example.stefan.mydigitalid.view.ContactsSectionFragment;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
public class ContactsIntentService extends IntentService
{
    public ContactsIntentService()
    {
        super("ContactsIntentService");
    }
    @Override
    protected void onHandleIntent(Intent intent)
    {
        String selection= ContactsContract.CommonDataKinds.Contactables.HAS_PHONE_NUMBER + "=" + 1;
        String sortBy= ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME + " COLLATE NOCASE ASC";
        Cursor data=getContentResolver().query(ContactsContract.CommonDataKinds.Contactables.CONTENT_URI,null,selection,null,sortBy);
        ArrayList<Contact> contactsArray = new ArrayList<>();
        ArrayList<Bitmap> contactImages=new ArrayList<>();
        String lookupKey = ""; //TODO maybe it should not be "" because a contact may have no name on some phones, maybe not possible
        Contact c = null;
        if (data!=null && data.getCount() > 0)
        {
            int nameColumnIndex = data.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME);
            int numberColumnIndex = data.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int emailColumnIndex = data.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
            int lookUpKeyColumnIndex = data.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.LOOKUP_KEY);
            int typeColumnIndex = data.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.MIMETYPE);
            String id="";
            int count=0;
            data.moveToFirst();
            do {
                String displayName = data.getString(nameColumnIndex);
                String currentLookupKey = data.getString(lookUpKeyColumnIndex);
                if (!currentLookupKey.equals(lookupKey))
                {
                    c = new Contact(displayName);
                    contactsArray.add(c);
                }
                String mimeType = data.getString(typeColumnIndex);
                if (mimeType.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                    String number = data.getString(numberColumnIndex).trim();
                    if (!c.phoneNumbers.contains(number))
                        c.phoneNumbers.add(number);
                } else if (mimeType.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                    String email = data.getString(emailColumnIndex).trim();
                    if (!c.emails.contains(email))
                        c.emails.add(email);
                }
                Uri uriContact = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, currentLookupKey);
                Cursor cursorID = getContentResolver().query(uriContact,
                        new String[]{ContactsContract.Contacts._ID},
                        null, null, null);
                if (cursorID!=null && cursorID.moveToFirst()) {

                    id = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
                }
                Bitmap photo = null;
                try {
                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                            getContentResolver(),
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));

                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                    }
                    if(!currentLookupKey.equals(lookupKey))
                    {
                        contactImages.add(photo);
                        count++;
                    }
                    else
                    {
                        if(contactImages.get(count-1)==null) //TODO maybe add another condition count-1>=0
                            contactImages.set(count-1,photo);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lookupKey = currentLookupKey;
            } while (data.moveToNext());
            Intent broadcastIntent=new Intent(ContactsSectionFragment.CONTACTS_BROADCAST_ACTION);
            broadcastIntent.putExtra(ContactsSectionFragment.CONTACTS_ARRAY,contactsArray);
            broadcastIntent.putExtra(ContactsSectionFragment.PICTURES_ARRAY,contactImages);
            this.sendBroadcast(broadcastIntent);
        }

    }

}
