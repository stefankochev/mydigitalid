package com.example.stefan.mydigitalid.model;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Stefan on 2/8/2018.
 */

public class AppInfo implements Serializable {

    public Drawable icon;
    public String packageName;
    public String label;
    public String size;

    public AppInfo(ResolveInfo resolveInfo, PackageManager packageManager){
        this.icon = resolveInfo.loadIcon(packageManager);
        this.label = resolveInfo.loadLabel(packageManager).toString();
        this.packageName = resolveInfo.activityInfo.packageName;
        this.size = parseBytes(getSize(resolveInfo.activityInfo.applicationInfo));
    }


    private static long getSize(ApplicationInfo info){
        File publicSourceDir = new File(info.publicSourceDir);

        return publicSourceDir.length();
    }

    private static String parseBytes(double bytes){
        String sizeString;

        if(bytes >= (1024*1024*1024)){
            sizeString = String.format("%.2fGB", bytes / (1024*1024*1024));
        } else if(bytes >= (1024*1024)){
            sizeString = String.format("%.2fMB", bytes / (1024*1024));
        } else if(bytes >= 1024){
            sizeString = String.format("%.2fKB", bytes / 1024);
        } else {
            sizeString = String.format("%.0fB", bytes);
        }

        return String.format("%s", sizeString);
    }

}
