package com.example.stefan.mydigitalid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Stefan on 2/17/2018.
 */

public class PermissionsStats implements Serializable {

    @SerializedName("camera")
    public Float camera;

    @SerializedName("location")
    public Float location;

    @SerializedName("microphone")
    public Float microphone;

}
