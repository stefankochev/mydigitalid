package com.example.stefan.mydigitalid.presenter;


import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.example.stefan.mydigitalid.loaders.AppPermissionsLoader;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.view.AppSectionPermissionsFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 2/5/2018.
 */

public class AppSectionPermissionsPresenter implements Serializable, LoaderManager.LoaderCallbacks<List<PermissionGroup>> {

    protected AppSectionPermissionsFragment mView;


    public void attachView(AppSectionPermissionsFragment view) {
        mView = view;
    }

    public void onRefresh(){
        mView.getLoaderManager().initLoader(2,null,this).forceLoad();
    }

    public void onDestroy(){
        this.mView = null;
    }

    @Override
    public Loader<List<PermissionGroup>> onCreateLoader(int id, Bundle args) {
        return new AppPermissionsLoader(mView.getActivity().getApplicationContext(),mView.getAppPackageName());
    }

    @Override
    public void onLoadFinished(Loader<List<PermissionGroup>> loader, List<PermissionGroup> data) {
        mView.setGroups(data);
    }

    @Override
    public void onLoaderReset(Loader<List<PermissionGroup>> loader) {
        if(mView!=null){
            mView.setGroups(new ArrayList<PermissionGroup>());
        }
    }
}
