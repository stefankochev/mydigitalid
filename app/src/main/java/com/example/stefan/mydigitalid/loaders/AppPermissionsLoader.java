package com.example.stefan.mydigitalid.loaders;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.support.v4.content.AsyncTaskLoader;

import com.example.stefan.mydigitalid.model.PermissionGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Stefan on 2/5/2018.
 */

public class AppPermissionsLoader extends AsyncTaskLoader<List<PermissionGroup>> {

    private String appPackageName;
    private static final String[] dangerousGroups = {
            Manifest.permission_group.CALENDAR,
            Manifest.permission_group.CAMERA,
            Manifest.permission_group.CONTACTS,
            Manifest.permission_group.LOCATION,
            Manifest.permission_group.MICROPHONE,
            Manifest.permission_group.PHONE,
            Manifest.permission_group.SENSORS,
            Manifest.permission_group.SMS,
            Manifest.permission_group.STORAGE
    };

    private static final String[] permissions = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_PHONE_NUMBERS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ANSWER_PHONE_CALLS,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.ADD_VOICEMAIL,
            Manifest.permission.USE_SIP,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.BODY_SENSORS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.RECEIVE_MMS,
            Manifest.permission.RECEIVE_WAP_PUSH,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public AppPermissionsLoader(Context context, String pkgName) {
        super(context);
        this.appPackageName = pkgName;
    }

    @Override
    public List<PermissionGroup> loadInBackground() {
        List<PermissionGroup> groupsList = new ArrayList<>();
        try {
            PackageInfo pi = getContext().getPackageManager().getPackageInfo(appPackageName, PackageManager.GET_PERMISSIONS);
            if(pi.requestedPermissions.length == 0){
                return new ArrayList<>();
            }
            else {
                List<String> groups = Arrays.asList(dangerousGroups);
                List<String> perms = Arrays.asList(permissions);
                for (int i=0;i<pi.requestedPermissions.length;i++){
                    String info = pi.requestedPermissions[i];
                    if(perms.contains(info)) {
                        PermissionInfo pinfo = getContext().getPackageManager().getPermissionInfo(info, PackageManager.GET_META_DATA);
                        if (groups.contains(pinfo.group)) {
                            PermissionGroupInfo g = getContext().getPackageManager().getPermissionGroupInfo(pinfo.group, PackageManager.GET_META_DATA);
                            PermissionGroup group = new PermissionGroup(g);
                            if ((pi.requestedPermissionsFlags[i] & PackageInfo.REQUESTED_PERMISSION_GRANTED) != 0){
                                group.granted = true;
                            }
                            if(!groupsList.contains(group)) {
                                groupsList.add(group);
                            }
                            else{
                                groupsList.remove(group);
                                groupsList.add(group);
                            }
                        }
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return groupsList;
    }
}
