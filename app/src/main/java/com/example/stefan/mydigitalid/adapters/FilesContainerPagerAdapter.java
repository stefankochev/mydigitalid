package com.example.stefan.mydigitalid.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.stefan.mydigitalid.presenter.FileSectionPresenter;
import com.example.stefan.mydigitalid.view.FileSectionFragment;
import com.example.stefan.mydigitalid.view.FilesStatisticsFragment;


/**
 * Created by kristijan on 2/21/18.
 */

public class FilesContainerPagerAdapter extends FragmentPagerAdapter {

    public FilesContainerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            FileSectionPresenter fPresenter = new FileSectionPresenter();
            return FileSectionFragment.newInstance(fPresenter);
        } else {
            return new FilesStatisticsFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "My Files";
        } else {
            return "Statistics";
        }
    }
}
