package com.example.stefan.mydigitalid.adapters;

import android.content.Context;
import android.provider.CallLog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.Call;
import com.example.stefan.mydigitalid.model.CallStatistic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jovan on 05-Feb-18.
 */

public class CallsItemAdapter extends RecyclerView.Adapter<CallsItemAdapter.CallsViewHolder>
{
    private LayoutInflater mLayoutInflater;
    private List<Call> mCallsList;
    private ArrayList<CallStatistic> callsStatistics;
    private Context mContext;
    public CallsItemAdapter(List<Call> calls, LayoutInflater layoutInflater,Context context)
    {
        mLayoutInflater=layoutInflater;
        mCallsList=calls;
        mContext=context;
    }
    @Override
    public CallsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=mLayoutInflater.inflate(R.layout.calls_row,parent,false);
        return new CallsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CallsViewHolder holder, int position)
    {
        holder.mCallerName.setText(mCallsList.get(position).callerName);
        holder.mCallerNumber.setText(mCallsList.get(position).number);
        holder.mCallDuration.setText(formatCallDuration(mCallsList.get(position).duration));
        holder.mCallDate.setText(mCallsList.get(position).date);
        if(Integer.parseInt(mCallsList.get(position).type)==CallLog.Calls.INCOMING_TYPE)
        {
            //holder.mCallImage.setImageResource(R.mipmap.incomming_call);
            holder.mCallImage.setImageResource(android.R.drawable.sym_call_incoming);
            holder.mCallerName.setTextColor(mContext.getResources().getColor(android.R.color.black));
        }
        else if(Integer.parseInt(mCallsList.get(position).type)==CallLog.Calls.OUTGOING_TYPE)
        {
            //holder.mCallImage.setImageResource(R.mipmap.outgoing_call);
            holder.mCallImage.setImageResource(android.R.drawable.sym_call_outgoing);
            holder.mCallerName.setTextColor(mContext.getResources().getColor(android.R.color.black));
        }
        else if(Integer.parseInt(mCallsList.get(position).type)==CallLog.Calls.MISSED_TYPE)
        {
            //holder.mCallImage.setImageResource(R.mipmap.missed_call);
            holder.mCallImage.setImageResource(android.R.drawable.sym_call_missed);
            holder.mCallerName.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
        }
    }

    @Override
    public int getItemCount()
    {
        return mCallsList.size();
    }

    public class CallsViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mCallImage;
        public TextView mCallerNumber;
        public TextView mCallDate;
        public TextView mCallDuration;
        public TextView mCallerName;
        public CallsViewHolder(View itemView)
        {
            super(itemView);
            mCallImage=itemView.findViewById(R.id.call_image);
            mCallerNumber=itemView.findViewById(R.id.caller_number);
            mCallDate=itemView.findViewById(R.id.call_date);
            mCallDuration=itemView.findViewById(R.id.call_duration);
            mCallerName=itemView.findViewById(R.id.caller_name);
        }
    }

    private String formatCallDuration(String duration)
    {
        int time= Integer.parseInt(duration);
        int hours=time/3600;
        time=time%3600;
        int minutes=time/60;
        time=time%60;
        String dur="";
        if(hours>0)
            dur+=hours+" h ";
        if(minutes>0)
            dur+=minutes+" min ";
        dur+=time+" sec ";
        return dur;
    }
    public void getCallsStatistics()
    {
        callsStatistics=new ArrayList<>();
        for (Call c:mCallsList)
        {
            if(c.callerName!=null)
            {
                CallStatistic callStatistic =new CallStatistic(c.callerName,1, Integer.parseInt(c.duration),1);
                if(!callsStatistics.contains(callStatistic))
                {
                    callsStatistics.add(callStatistic);
                }
                else
                {
                    if(Integer.parseInt(c.type)==CallLog.Calls.OUTGOING_TYPE)
                        callsStatistics.get(callsStatistics.indexOf(callStatistic)).numberOfTimesCalled++;
                    callsStatistics.get(callsStatistics.indexOf(callStatistic))
                            .timeOfPhoneCalls+=Integer.parseInt(c.duration);
                    callsStatistics.get(callsStatistics.indexOf(callStatistic)).numberOfPhoneCalls++;

                }
            }
        }
        for (CallStatistic c:callsStatistics)
        {
            Log.e("AAAAA",c.callerName+" "+c.numberOfTimesCalled + " "+c.timeOfPhoneCalls +" "+ c.numberOfPhoneCalls);
        }
    }
    public ArrayList<CallStatistic> getMostFrequentlyCalled()
    {
        getCallsStatistics();
        Collections.sort(callsStatistics);
        Log.e("AAAA","1");
        for (CallStatistic c:callsStatistics)
        {
            Log.e("AAAAA",c.callerName+" "+c.numberOfTimesCalled);
        }
        return callsStatistics;
    }
    public ArrayList<CallStatistic> getMostTimeTalked()
    {
        getCallsStatistics();
        for(int i=0;i<callsStatistics.size();i++)
        {
            for(int j=0;j<callsStatistics.size()-i-1;j++)
            {
                if(callsStatistics.get(j).timeOfPhoneCalls<callsStatistics.get(j+1).timeOfPhoneCalls)
                {
                    CallStatistic c=callsStatistics.get(j);
                    callsStatistics.set(j,callsStatistics.get(j+1));
                    callsStatistics.set(j+1,c);
                }
            }
        }
        /*Log.e("AAAA","2");
        for (CallStatistic c:callsStatistics)
        {
            Log.e("AAAAA",c.callerName+" "+c.timeOfPhoneCalls);
        }*/
        return callsStatistics;
    }
    public ArrayList<CallStatistic> getMostAveragetTimeTalked()
    {
        getCallsStatistics();
        for(int i=0;i<callsStatistics.size();i++)
        {
            for(int j=0;j<callsStatistics.size()-i-1;j++)
            {
                if(callsStatistics.get(j).timeOfPhoneCalls/callsStatistics.get(j).numberOfPhoneCalls<
                        callsStatistics.get(j+1).timeOfPhoneCalls/callsStatistics.get(j+1).numberOfPhoneCalls)
                {
                    CallStatistic c=callsStatistics.get(j);
                    callsStatistics.set(j,callsStatistics.get(j+1));
                    callsStatistics.set(j+1,c);
                }
            }
        }
        Log.e("AAAA","3");
        for (CallStatistic c:callsStatistics)
        {
            Log.e("AAAAA",c.callerName+" "+(c.timeOfPhoneCalls/c.numberOfPhoneCalls));
        }
        return callsStatistics;
    }
}
