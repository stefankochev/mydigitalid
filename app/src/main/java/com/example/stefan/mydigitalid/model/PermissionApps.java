package com.example.stefan.mydigitalid.model;

import android.content.pm.ResolveInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionApps implements Serializable {

    public List<BasicAppInfo> apps;

    public PermissionApps(List<BasicAppInfo> apps){
        this.apps = apps;
    }
}
