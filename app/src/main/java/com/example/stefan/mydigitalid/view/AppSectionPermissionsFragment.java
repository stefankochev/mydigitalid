package com.example.stefan.mydigitalid.view;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.AppPermissionsAdapter;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.presenter.AppSectionPermissionsPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 1/31/2018.
 */

public class AppSectionPermissionsFragment extends Fragment {

    private static final String ARG_APPS_PERMISSIONS_PRESENTER = "applications.permissions.presenter";
    private AppSectionPermissionsPresenter mPresenter;
    private String appPackageName;
    private String appName;

    private List<PermissionGroup> groups = new ArrayList<>();
    private AppPermissionsAdapter adapter;
    private RecyclerView recyclerView;

    private TextView noItemsText;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if(arguments != null) {
            mPresenter = (AppSectionPermissionsPresenter) arguments.getSerializable(ARG_APPS_PERMISSIONS_PRESENTER);
            appPackageName = (String) arguments.getSerializable("package");
            appName = arguments.getString("label");
        }
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_apps_permissions,container,false);

        noItemsText = (TextView)view.findViewById(R.id.no_items_label);
        noItemsText.setVisibility(View.GONE);

        TextView label = (TextView)view.findViewById(R.id.label);
        label.setText(appName);

        ImageButton upButton = (ImageButton)view.findViewById(R.id.upButton);
        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        adapter = new AppPermissionsAdapter(groups);
        recyclerView = (RecyclerView)view.findViewById(R.id.permission_groups_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);


        if(mPresenter!=null) {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }

        return view;
    }

    @Override
    public void onResume() {
        mPresenter.attachView(this);
        mPresenter.onRefresh();
        super.onResume();
    }

    @Override
    public void onPause() {
        mPresenter.onDestroy();
        super.onPause();
    }



    public void setGroups(List<PermissionGroup> groupList){
        if(groupList.isEmpty()){
            noItemsText.setVisibility(View.VISIBLE);
        }
        else{
            groups.clear();
            groups.addAll(groupList);
            adapter.notifyDataSetChanged();
        }
    }

    public static AppSectionPermissionsFragment newInstance(AppSectionPermissionsPresenter presenter,String appPackageName,String label){
        Bundle args = new Bundle();
        args.putSerializable(ARG_APPS_PERMISSIONS_PRESENTER, presenter);
        args.putSerializable("package",appPackageName);
        args.putString("label",label);
        AppSectionPermissionsFragment fragment = new AppSectionPermissionsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public String getAppPackageName() {
        return appPackageName;
    }
}
