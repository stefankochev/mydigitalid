package com.example.stefan.mydigitalid.retrofit;

import com.example.stefan.mydigitalid.model.FileAverageStats;
import com.example.stefan.mydigitalid.model.PermissionsStats;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Stefan on 2/17/2018.
 */

public interface PermissionsApiInterface {

    @GET("getPermissionsStatistics")
    Call<PermissionsStats> getStats();

    @GET("getFilesStatistics")
    Call<FileAverageStats> getFilesStats();

}
