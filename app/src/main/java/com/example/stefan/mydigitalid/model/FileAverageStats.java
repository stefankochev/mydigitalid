package com.example.stefan.mydigitalid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kristijan on 2/21/18.
 */

public class FileAverageStats implements Serializable {

    @SerializedName("audio")
    public Float audio;

    @SerializedName("image")
    public Float image;

    @SerializedName("text")
    public Float text;

    @SerializedName("video")
    public Float video;


}
