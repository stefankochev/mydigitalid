package com.example.stefan.mydigitalid.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.stefan.mydigitalid.MainActivity;
import com.example.stefan.mydigitalid.presenter.AccountSectionPresenter;
import com.example.stefan.mydigitalid.presenter.AppSectionPresenter;
import com.example.stefan.mydigitalid.presenter.CallsSectionPresenter;
import com.example.stefan.mydigitalid.presenter.ContactsSectionPresenter;
import com.example.stefan.mydigitalid.presenter.FileSectionPresenter;
import com.example.stefan.mydigitalid.presenter.PermissionsPresenter;
import com.example.stefan.mydigitalid.view.AccountSectionFragment;
import com.example.stefan.mydigitalid.view.AppSectionFragment;
import com.example.stefan.mydigitalid.view.CallsSectionFragment;
import com.example.stefan.mydigitalid.view.ContactsSectionFragment;
import com.example.stefan.mydigitalid.view.FileSectionFragment;
import com.example.stefan.mydigitalid.view.FilesContainerFragment;
import com.example.stefan.mydigitalid.view.FilesStatisticsFragment;
import com.example.stefan.mydigitalid.view.PermissionsContainerFragment;
import com.example.stefan.mydigitalid.view.PermissionsSectionFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Stefan on 1/16/2018.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {

            case 0:
                AppSectionPresenter presenter = new AppSectionPresenter();
                AppSectionFragment appSectionFragment = AppSectionFragment.newInstance(presenter);
                return appSectionFragment;

            case 1:
                return new FilesContainerFragment();

            case 2:
                AccountSectionPresenter accPresenter = new AccountSectionPresenter();
                AccountSectionFragment accountSectionFragment = AccountSectionFragment.newInstance(accPresenter);
                return accountSectionFragment;

            case 3:
                ContactsSectionPresenter contactsSectionPresenter=new ContactsSectionPresenter();
                ContactsSectionFragment contactsSectionFragment=ContactsSectionFragment.newInstance(contactsSectionPresenter);
                return contactsSectionFragment;

            case 4:
                CallsSectionPresenter callsSectionPresenter=new CallsSectionPresenter();
                CallsSectionFragment callsSectionFragment=CallsSectionFragment.newInstance(callsSectionPresenter);
                return callsSectionFragment;

            case 5:
                /*PermissionsPresenter permissionsPresenter = new PermissionsPresenter();
                PermissionsSectionFragment permissionsSectionFragment = PermissionsSectionFragment.newInstance(permissionsPresenter);
                return permissionsSectionFragment;*/

                return new PermissionsContainerFragment();

            default:
                // The other sections of the app are dummy placeholders.
                Fragment fragment = new MainActivity.DummySectionFragment();
                Bundle args = new Bundle();
                args.putInt(MainActivity.DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                fragment.setArguments(args);
                return fragment;
        }
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String[] titles = {"Apps", "Files", "Accounts","Contacts","Calls","Permissions"};
        if(position < titles.length)
            return titles[position];
        else
            return "Section" + position;

    }
}