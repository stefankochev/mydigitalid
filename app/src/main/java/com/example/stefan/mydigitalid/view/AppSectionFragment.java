package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.AppItemAdapter;
import com.example.stefan.mydigitalid.model.AppInfo;
import com.example.stefan.mydigitalid.presenter.AppSectionPresenter;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;


import java.util.ArrayList;
import java.util.List;



/**
 * Created by Stefan on 1/16/2018.
 */

public class AppSectionFragment extends Fragment {

    private static final String ARG_APPS_PRESENTER = "applications.presenter";

    private AppSectionPresenter mPresenter;

    private List<AppInfo> appsList = new ArrayList<>();
    private AppItemAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    TouchScrollBar touchScrollBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if(arguments != null) {
            mPresenter = (AppSectionPresenter) arguments.getSerializable(ARG_APPS_PRESENTER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_apps,container,false);

        touchScrollBar = (TouchScrollBar)view.findViewById(R.id.touchScrollBar);
        touchScrollBar.setIndicator(new AlphabetIndicator(getActivity().getApplicationContext()),false);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshApps);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mPresenter != null)
                    mPresenter.reload();
            }
        });

        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerViewApps);
        mAdapter = new AppItemAdapter(appsList,getActivity(),mRecyclerView,getActivity());
        progressBar = (ProgressBar)view.findViewById(R.id.appsProgressBar);
        progressBar.setVisibility(View.GONE);




        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);

        if(mPresenter!=null) {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }

        return view;
    }



    @Override
    public void onResume() {
        mPresenter.attachView(this);
        mPresenter.reload();
        super.onResume();
    }

    @Override
    public void onPause() {
        mPresenter.onDestroy();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.onDestroy();
        super.onSaveInstanceState(outState);
    }

    public void setAppsList(List<AppInfo> apps){
        appsList.clear();
        appsList.addAll(apps);
        mAdapter.notifyDataSetChanged();
        this.swipeRefreshLayout.setRefreshing(false);
    }

    public static AppSectionFragment newInstance(AppSectionPresenter presenter){
        Bundle args = new Bundle();
        args.putSerializable(ARG_APPS_PRESENTER, presenter);

        AppSectionFragment fragment = new AppSectionFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public void showProgressBar(){progressBar.setVisibility(View.VISIBLE);}
    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

}
