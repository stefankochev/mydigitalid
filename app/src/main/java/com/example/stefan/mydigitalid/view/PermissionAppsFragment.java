package com.example.stefan.mydigitalid.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.PermissionAppsAdapter;
import com.example.stefan.mydigitalid.model.PermissionApps;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionAppsFragment extends Fragment {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.apps_for_permission,container,false);

        PermissionApps permissionApps = (PermissionApps) getArguments().getSerializable("permission_apps");

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.permission_apps_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        PermissionAppsAdapter adapter = new PermissionAppsAdapter(permissionApps.apps,getActivity().getPackageManager(),getActivity().getApplicationContext());
        recyclerView.setAdapter(adapter);

        return view;
    }

}
