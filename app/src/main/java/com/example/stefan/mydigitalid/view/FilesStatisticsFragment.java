package com.example.stefan.mydigitalid.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.FileAverageStats;
import com.example.stefan.mydigitalid.model.FileStats;
import com.example.stefan.mydigitalid.model.PermissionsStats;
import com.example.stefan.mydigitalid.retrofit.PermissionsApiClient;
import com.example.stefan.mydigitalid.retrofit.PermissionsApiInterface;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kristijan on 2/21/18.
 */

public class FilesStatisticsFragment extends Fragment {

    private SwipeRefreshLayout refreshLayout;
    private BarChart barChart;
//    private ProgressBar progressBar;

    private ConnectionBroadcastReceiver connectionBroadcastReceiver;
    public ImageView noConnection;
    AnimatedVectorDrawable avd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionBroadcastReceiver = new ConnectionBroadcastReceiver();
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        getContext().registerReceiver(connectionBroadcastReceiver, filter);
        //checkConnection();
        super.onResume();
    }

    @Override
    public void onPause() {
        getContext().unregisterReceiver(connectionBroadcastReceiver);
        super.onPause();
    }

    public boolean checkConnection(){
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if(!connected) {
            if (noConnection != null && avd != null) {
                noConnection.setVisibility(View.VISIBLE);
                barChart.setVisibility(View.GONE);
                noConnection.setImageDrawable(avd);
                avd.start();
            }
        }
        else{
//            showProgressBar();
            noConnection.setVisibility(View.GONE);
            refreshChart();
            barChart.setVisibility(View.VISIBLE);
        }
        refreshLayout.setRefreshing(false);
        return connected;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_files_statistics, container, false);

        barChart = view.findViewById(R.id.FilesStatisticsBarChart);
        refreshLayout = view.findViewById(R.id.FilesStatisticsRefreshLayout);
//        progressBar = view.findViewById(R.id.FilesStatisticsProgressBar);
//        progressBar.setVisibility(ProgressBar.GONE);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
                boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
                if(connected){
                    refreshChart();
                }
                else {
                    Toast.makeText(getContext(), "Connect to the internet to refresh", Toast.LENGTH_SHORT).show();
                    refreshLayout.setRefreshing(false);
                }
            }
        });


        final ViewStub stub = view.findViewById(R.id.stub_no_connection);
        avd = (AnimatedVectorDrawable) getContext().getDrawable(R.drawable.avd_no_connection);

        noConnection = (ImageView) stub.inflate();
        noConnection.setVisibility(View.GONE);
        checkConnection();
//        refreshChart();

        return view;
    }

    private void refreshChart() {

        PermissionsApiInterface apiService =
                PermissionsApiClient.getClient().create(PermissionsApiInterface.class);

        Call<FileAverageStats> call = apiService.getFilesStats();

        call.enqueue(new Callback<FileAverageStats>() {
            @Override
            public void onResponse(Call<FileAverageStats> call, Response<FileAverageStats> response) {
                FileAverageStats stats = response.body();
                if(stats!=null){
                    fillChart(stats);
                }
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<FileAverageStats> call, Throwable t) {
                Log.d("RETROFIT_FAIL", "FAIL");
            }
        });
    }

    private void fillChart(FileAverageStats averageStats) {
        SharedPreferences sharedPref = getContext().getSharedPreferences("com.example.stefan.mydigitalid.PREFERENCES_FILE", Context.MODE_PRIVATE);
        float myAudioSize = sharedPref.getFloat("audio", 0);
        float myImageSize = sharedPref.getFloat("image", 0);
        float myTextSize = sharedPref.getFloat("text", 0);
        float myVideoSize = sharedPref.getFloat("video", 0);

        List<BarEntry> myEntries = new ArrayList<>();
        List<BarEntry> averageEntries = new ArrayList<>();

        myEntries.add(new BarEntry(0, myAudioSize));
        myEntries.add(new BarEntry(1, myImageSize));
        myEntries.add(new BarEntry(2, myTextSize));
        myEntries.add(new BarEntry(3, myVideoSize));

        averageEntries.add(new BarEntry(0, averageStats.audio));
        averageEntries.add(new BarEntry(1, averageStats.image));
        averageEntries.add(new BarEntry(2, averageStats.text));
        averageEntries.add(new BarEntry(3, averageStats.video));

        BarDataSet myDataSet = new BarDataSet(myEntries, "Total size of my files");
        BarDataSet averageDataSet = new BarDataSet(averageEntries, "Average total size of files");

        myDataSet.setColor(Color.parseColor("#7CB342")); // colorPrimaryLight
        averageDataSet.setColor(Color.parseColor("#33691E")); // colorPrimaryDark

        MyValueFormatter valueFormatter = new MyValueFormatter();
        myDataSet.setValueFormatter(valueFormatter);
        averageDataSet.setValueFormatter(valueFormatter);

        float groupSpace = 0.2f;
        float barSpace = 0.1f; // x2 dataset
        float barWidth = 0.8f; // x2 dataset

        String[] labels = {"audio", "image", "text", "video"};

        BarData data = new BarData(myDataSet, averageDataSet);
        data.setBarWidth(barWidth); // set the width of each bar
        barChart.setData(data);
        barChart.groupBars(-0.0f, groupSpace, barSpace); // perform the "explicit" grouping
        barChart.setFitBars(false);
        barChart.setHorizontalScrollBarEnabled(true);
        barChart.setDrawGridBackground(false);
        barChart.setPinchZoom(false);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setClickable(false);
        barChart.getData().setHighlightEnabled(false);
        Description description = new Description();
        description.setEnabled(false);
        barChart.setDescription(description);
        Legend legend = barChart.getLegend();


        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new LabelFormatter(labels));
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(8);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis left = barChart.getAxisLeft();
        left.setDrawZeroLine(false);
        left.setDrawLabels(false);
        left.setDrawGridLines(false);
        left.setAxisMinimum(0f);

        YAxis right = barChart.getAxisRight();
        right.setDrawZeroLine(false);
        right.setDrawLabels(false);
        right.setDrawGridLines(false);
        right.setAxisMinimum(0f);

        barChart.invalidate(); // refresh
    }

    private class LabelFormatter implements IAxisValueFormatter {
        private final String[] mLabels;

        public LabelFormatter(String[] labels) {
            mLabels = labels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            int val = (int) value;
            if (val % 2 == 1 && (val - 1) / 2 < mLabels.length) {
                return mLabels[(val - 1) / 2]; //odd number to array index
            } else return "";
        }
    }

    public class MyValueFormatter implements IValueFormatter {

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            String sizeString;
            float size = value;

            if (size >= (1024 * 1024 * 1024)) {
                sizeString = String.format(Locale.US,"%.2fGB", size / (1024 * 1024 * 1024));
            } else if (size >= (1024 * 1024)) {
                sizeString = String.format(Locale.US,"%.2fMB", size / (1024 * 1024));
            } else if (size >= 1024) {
                sizeString = String.format(Locale.US,"%.2fKB", size / 1024);
            } else {
                sizeString = String.format(Locale.US,"%.0fB", size);
            }

            return sizeString;
        }
    }

    public class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final NetworkInfo activeNetworkInfo = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            if(connected){
                checkConnection();
            }
        }
    }

}
