package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.PermissionAppsPagerAdapter;
import com.example.stefan.mydigitalid.model.PermissionGroup;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionSectionAppsFragment extends Fragment {

    private ViewPager viewPager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_permission_apps,container,false);


        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar_permission_apps);
        TabLayout tabLayout = (TabLayout)view.findViewById(R.id.tabs_apps);
        viewPager = (ViewPager)view.findViewById(R.id.pager_apps_permission);


        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        PermissionGroup permissionGroup = (PermissionGroup) getArguments().getSerializable("permission_group");
        toolbar.setTitle(permissionGroup.labelRes);

        PermissionAppsPagerAdapter pagerAdapter = new PermissionAppsPagerAdapter(getChildFragmentManager(), permissionGroup);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);

        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

}
