package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.FileStats;
import com.example.stefan.mydigitalid.presenter.FileSectionPresenter;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.graphics.Color;

/**
 * Created by kristijan on 1/20/18.
 */

public class FileSectionFragment extends Fragment {

    private static final String ARG_FILES_PRESENTER = "files.presenter";

    private FileSectionPresenter mPresenter;

//    private FileItemAdapter mAdapter;
//    private RecyclerView mRecyclerView;
    private TextView numberOfImages, numberOfAudios, numberOfVideos, numberOfTexts;
    private TextView sizeImages, sizeAudio, sizeVideo, sizeText;

    private RelativeLayout statsLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    PieChart pieChartMemory;
    private List<FileStats> stats;

    private ProgressBar progressBar;


    private FileStats getStatsByType(String type){
        for(FileStats stat : stats){
            if(stat.type.equals(type))
                return stat;
        }
        return new FileStats("", 0, 0);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if(arguments != null) {
            mPresenter = (FileSectionPresenter) arguments.getSerializable(ARG_FILES_PRESENTER);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_file, container, false);

        progressBar = (ProgressBar)view.findViewById(R.id.files_progress_bar);
        progressBar.setVisibility(View.GONE);

        numberOfImages = view.findViewById(R.id.num_images);
        numberOfAudios = view.findViewById(R.id.num_audios);
        numberOfVideos = view.findViewById(R.id.num_videos);
        numberOfTexts = view.findViewById(R.id.num_text);

        sizeImages = view.findViewById(R.id.size_images);
        sizeAudio = view.findViewById(R.id.size_audio);
        sizeVideo = view.findViewById(R.id.size_video);
        sizeText = view.findViewById(R.id.size_text);

        statsLayout = view.findViewById(R.id.stats);
        statsLayout.setVisibility(View.INVISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mPresenter != null)
                    mPresenter.reload();
            }
        });

        pieChartMemory = view.findViewById(R.id.pieChartMemory);
        pieChartMemory.setCenterTextColor(Color.BLACK);
        pieChartMemory.setUsePercentValues(true);
        pieChartMemory.setEntryLabelColor(Color.BLACK);
        pieChartMemory.setCenterText("Memory usage %");
//        Description pieChartDesription = new Description();
//        pieChartDesription.setText("Number of bytes per type");
//        pieChartDesription.setTextSize(12);
//        pieChartMemory.setDescription(pieChartDesription);
        pieChartMemory.setRotationEnabled(false);
        pieChartMemory.getLegend().setEnabled(false);
        pieChartMemory.getDescription().setEnabled(false);
        pieChartMemory.setNoDataText("");

        

//        mRecyclerView = view.findViewById(R.id.recyclerViewFiles);
//        mAdapter = new FileItemAdapter(filesList, getActivity().getApplicationContext());

//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager
//                (getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(mAdapter);

        if(mPresenter!=null) {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }

        return view;
    }

    @Override
    public void onResume() {
        mPresenter.attachView(this);
        mPresenter.reload();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.onDestroy();
        super.onSaveInstanceState(outState);
    }


    public static FileSectionFragment newInstance(FileSectionPresenter presenter){
        Bundle args = new Bundle();
        args.putSerializable(ARG_FILES_PRESENTER, presenter);

        FileSectionFragment fragment = new FileSectionFragment();
        fragment.setArguments(args);

        return fragment;
    }


    public void setFiles(List<FileStats> stats){
        this.stats = stats;
        FileStats images = getStatsByType("image");
        FileStats audio = getStatsByType("audio");
        FileStats video = getStatsByType("video");
        FileStats text = getStatsByType("text");

        numberOfImages.setText(images.numberOfFiles + " files");
        numberOfAudios.setText(audio.numberOfFiles + " files");
        numberOfVideos.setText(video.numberOfFiles + " files");
        numberOfTexts.setText(text.numberOfFiles + " files");

        sizeImages.setText("size: "+ images.getSizeString());
        sizeVideo.setText("size: " + video.getSizeString());
        sizeAudio.setText("size: " + audio.getSizeString());
        sizeText.setText("size: " + text.getSizeString());

        statsLayout.setVisibility(View.VISIBLE);
        fillPieChartMemory();

        this.swipeRefreshLayout.setRefreshing(false);

    }



    private void fillPieChartMemory(){

        List<PieEntry> entries = new ArrayList<>();

        for(FileStats stat : stats){
            if(stat.totalSize > 0) {
                entries.add(new PieEntry(stat.totalSize, stat.type));
            }
        }

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setColors(new int[]{R.color.colorPrimary,R.color.colorPrimaryDark,R.color.colorPrimaryLight,R.color.colorAccentLight},this.getContext());
        dataSet.setValueFormatter(new PercentFormatter());

        dataSet.setValueTextSize(12);

        PieData pieData = new PieData(dataSet);
        pieChartMemory.setData(pieData);
        pieChartMemory.invalidate();

    }

    public void showProgressBar(){progressBar.setVisibility(View.VISIBLE);}
    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }


}
