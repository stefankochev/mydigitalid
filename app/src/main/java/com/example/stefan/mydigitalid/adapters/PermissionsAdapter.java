package com.example.stefan.mydigitalid.adapters;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.view.PermissionSectionAppsFragment;

import java.util.List;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionsAdapter extends RecyclerView.Adapter<PermissionsAdapter.PermissionHolder> {

    private List<PermissionGroup> groups;
    private FragmentActivity fragmentActivity;

    public class PermissionHolder extends RecyclerView.ViewHolder{

        public ImageView icon;
        public TextView name;
        public TextView info;

        public PermissionHolder(View itemView) {
            super(itemView);
            icon = (ImageView)itemView.findViewById(R.id.permission_group_icon);
            name = (TextView)itemView.findViewById(R.id.permission_group_name);
            info = (TextView)itemView.findViewById(R.id.permission_info);
        }
    }

    public PermissionsAdapter(List<PermissionGroup> list, FragmentActivity activity){
        this.groups = list;
        this.fragmentActivity = activity;
    }


    @Override
    public PermissionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.permission_row,parent,false);
        return new PermissionHolder(view);
    }

    @Override
    public void onBindViewHolder(final PermissionHolder holder, final int position) {
        final PermissionGroup permissionGroup = groups.get(position);

        holder.icon.setImageResource(permissionGroup.iconRes);
        holder.name.setText(permissionGroup.labelRes);

        int all = permissionGroup.allApps.size();
        int allowed  =permissionGroup.appsGranted.size();
        String info = String.format("%d of %d apps allowed",allowed,all);
        holder.info.setText(info);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
               // PermissionSectionAppsPresenter presenter = new PermissionSectionAppsPresenter(permissionGroup);
                PermissionSectionAppsFragment fragment = new PermissionSectionAppsFragment();
                Bundle args = new Bundle();
                args.putSerializable("permission_group",permissionGroup);

                fragment.setArguments(args);

                fragmentTransaction.setCustomAnimations(R.anim.slide_from_right,R.anim.exit_to_right,R.anim.slide_from_right,R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.drawer_layout,fragment);
                fragmentTransaction.addToBackStack(permissionGroup.labelRes.toString());
                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

}
