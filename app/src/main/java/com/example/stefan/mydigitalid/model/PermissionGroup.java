package com.example.stefan.mydigitalid.model;

import android.content.pm.PermissionGroupInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 2/5/2018.
 */

public class PermissionGroup implements Serializable{

    public String groupName;
    public Integer iconRes;
    public Integer labelRes;
    public Boolean granted;
    public List<BasicAppInfo> appsGranted;
    public List<BasicAppInfo> allApps;

    public PermissionGroup(String groupName){
        this.groupName = groupName;
        iconRes = 0;
        labelRes = 0;
        granted = false;
        appsGranted = new ArrayList<>();
        allApps = new ArrayList<>();
    }

    public PermissionGroup(PermissionGroupInfo permissionGroupInfo){
        this.groupName = permissionGroupInfo.name;
        this.iconRes = permissionGroupInfo.icon;
        this.labelRes = permissionGroupInfo.labelRes;
        granted = false;
        appsGranted = new ArrayList<>();
        allApps = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        PermissionGroup group = (PermissionGroup)obj;
        return this.groupName.equals(group.groupName);
    }

    public boolean isGranted() {
        return granted;
    }
}
