package com.example.stefan.mydigitalid.presenter;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.example.stefan.mydigitalid.loaders.FileLoader;
import com.example.stefan.mydigitalid.model.FileStats;
import com.example.stefan.mydigitalid.view.AppSectionFragment;
import com.example.stefan.mydigitalid.view.FileSectionFragment;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kristijan on 1/20/18.
 */

public class FileSectionPresenter implements Serializable, LoaderManager.LoaderCallbacks<List<FileStats>>{

    protected FileSectionFragment mView;
    private int FILE_LOADER_ID = 0;

    public void attachView(FileSectionFragment view) {
        mView = view;
    }

    public void onDestroy(){
        this.mView = null;
    }

    public void onRefresh(){
        mView.showProgressBar();
        mView.getLoaderManager()
                .initLoader(FILE_LOADER_ID, null, this)
                .forceLoad();
    }

    public void reload(){
        mView.getLoaderManager()
                .restartLoader(FILE_LOADER_ID,null, this)
                .forceLoad();
    }

    @Override
    public Loader<List<FileStats>> onCreateLoader(int id, Bundle args) {
        return new FileLoader(mView.getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<FileStats>> loader, List<FileStats> data) {
        mView.hideProgressBar();
        mView.setFiles(data);
    }

    @Override
    public void onLoaderReset(Loader<List<FileStats>> loader) {
        if(mView!=null) {
            mView.setFiles(Collections.<FileStats>emptyList());
        }
    }
}
