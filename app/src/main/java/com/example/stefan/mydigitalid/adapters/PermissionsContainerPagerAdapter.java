package com.example.stefan.mydigitalid.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.stefan.mydigitalid.MainActivity;
import com.example.stefan.mydigitalid.presenter.PermissionsPresenter;
import com.example.stefan.mydigitalid.view.PermissionsSectionFragment;
import com.example.stefan.mydigitalid.view.PermissionsStatsFragment;

/**
 * Created by Stefan on 2/16/2018.
 */

public class PermissionsContainerPagerAdapter extends FragmentPagerAdapter {

    public PermissionsContainerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            PermissionsPresenter permissionsPresenter = new PermissionsPresenter();
            PermissionsSectionFragment permissionsSectionFragment = PermissionsSectionFragment.newInstance(permissionsPresenter);
            return permissionsSectionFragment;
        }
        else{
            return new PermissionsStatsFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "List";
        }
        else{
            return "Privacy";
        }
    }
}
