package com.example.stefan.mydigitalid.model;

import java.util.Locale;

/**
 * Created by kristijan on 1/31/18.
 */

public class FileStats {
    public String type;
    public int numberOfFiles;
    public long totalSize;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumberOfFiles() {
        return numberOfFiles;
    }

    public void setNumberOfFiles(int numberOfFiles) {
        this.numberOfFiles = numberOfFiles;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public FileStats(){
        type = "notype";
        numberOfFiles = 0;
        totalSize = 0;
    }

    public FileStats(String type, int numberOfFiles, long totalSize){
        this.type = type;
        this.numberOfFiles = numberOfFiles;
        this.totalSize = totalSize;
    }

    @Override
    public String toString(){
        return String.format(Locale.US,"%s: files: %d, size: %s", type, numberOfFiles, this.getSizeString());
    }

    public String getSizeString(){
        String sizeString;
        float size = totalSize;

        if(size >= (1024*1024*1024)){
            sizeString = String.format(Locale.US,"%.2fGB", size / (1024*1024*1024));
        } else if(size >= (1024*1024)){
            sizeString = String.format(Locale.US,"%.2fMB", size / (1024*1024));
        } else if(size >= 1024){
            sizeString = String.format(Locale.US,"%.2fKB", size / 1024);
        } else {
            sizeString = String.format(Locale.US,"%.0fB", size);
        }
        return sizeString;
    }
}
