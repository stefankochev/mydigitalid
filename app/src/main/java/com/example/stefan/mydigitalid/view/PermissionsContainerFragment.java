package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.PermissionsContainerPagerAdapter;

/**
 * Created by Stefan on 2/16/2018.
 */

public class PermissionsContainerFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_permissions_container,container,false);

        TabLayout tabLayout = view.findViewById(R.id.tabs_permissions_container);
        ViewPager viewPager = view.findViewById(R.id.pager_permissions_container);
        PermissionsContainerPagerAdapter adapter = new PermissionsContainerPagerAdapter(getFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
}
