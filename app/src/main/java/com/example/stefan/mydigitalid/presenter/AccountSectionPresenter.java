package com.example.stefan.mydigitalid.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.example.stefan.mydigitalid.view.AccountSectionFragment;

import java.io.Serializable;

/**
 * Created by Stefan on 1/21/2018.
 */

public class AccountSectionPresenter implements Serializable{

    protected AccountSectionFragment mView;

    public void attachView(AccountSectionFragment view) {
        mView = view;
    }

    public void onDestroy(){
        this.mView = null;
    }

    public void onRefresh(){

        Account[] accounts = AccountManager.get(mView.getActivity().getApplicationContext()).getAccounts();
        mView.setAccountsList(accounts);
    }

}
