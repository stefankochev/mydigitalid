package com.example.stefan.mydigitalid.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;

import java.io.File;
import java.util.List;

/**
 * Created by kristijan on 1/20/18.
 */

public class FileItemAdapter extends RecyclerView.Adapter<FileItemAdapter.ViewHolder> {

    private List<File> list;
    private Context context;
    private LayoutInflater layoutInflater;
    private PackageManager packageManager;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;

        public ViewHolder(View itemView){
            super(itemView);

            textView = itemView.findViewById(R.id.fileName);
        }

    }

    public FileItemAdapter(List<File> list, Context context){

        this.list = list;
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.packageManager = context.getPackageManager();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_row, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        File file = list.get(position);
        holder.textView.setText(file.getAbsolutePath());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
