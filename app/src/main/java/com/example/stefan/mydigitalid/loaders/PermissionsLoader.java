package com.example.stefan.mydigitalid.loaders;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ResolveInfo;
import android.support.v4.content.AsyncTaskLoader;

import com.example.stefan.mydigitalid.model.BasicAppInfo;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Stefan on 2/8/2018.
 */

public class PermissionsLoader extends AsyncTaskLoader<List<PermissionGroup>> {

    private static final String[] dangerousGroups = {
            Manifest.permission_group.CALENDAR,
            Manifest.permission_group.CAMERA,
            Manifest.permission_group.CONTACTS,
            Manifest.permission_group.LOCATION,
            Manifest.permission_group.MICROPHONE,
            Manifest.permission_group.PHONE,
            Manifest.permission_group.SENSORS,
            Manifest.permission_group.SMS,
            Manifest.permission_group.STORAGE
    };

    private static final String[] permissions = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_PHONE_NUMBERS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ANSWER_PHONE_CALLS,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.ADD_VOICEMAIL,
            Manifest.permission.USE_SIP,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.BODY_SENSORS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.RECEIVE_MMS,
            Manifest.permission.RECEIVE_WAP_PUSH,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public PermissionGroup microphone = new PermissionGroup(Manifest.permission_group.MICROPHONE);
    public PermissionGroup location = new PermissionGroup(Manifest.permission_group.LOCATION);
    public PermissionGroup camera = new PermissionGroup(Manifest.permission_group.CAMERA);


    public PermissionsLoader(Context context) {
        super(context);
    }

    @Override
    public List<PermissionGroup> loadInBackground() {

        List<PermissionGroup> resultList = new ArrayList<>();

        List<String> groups = Arrays.asList(dangerousGroups);
        List<String> perms = Arrays.asList(permissions);

        Intent query = new Intent(Intent.ACTION_MAIN);
        query.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfoList = getContext().getPackageManager().queryIntentActivities(query, PackageManager.MATCH_ALL);

        List<PermissionGroupInfo> allgroups = getContext().getPackageManager().getAllPermissionGroups(PackageManager.GET_META_DATA);

        for(PermissionGroupInfo permissionGroupInfo : allgroups) {
            if (groups.contains(permissionGroupInfo.name)) {
                PermissionGroup group = new PermissionGroup(permissionGroupInfo);

                for (ResolveInfo applicationInfo : resolveInfoList) {
                    try {
                        PackageInfo pi = getContext().getPackageManager().getPackageInfo(applicationInfo.activityInfo.packageName, PackageManager.GET_PERMISSIONS);
                        if (pi.requestedPermissions != null) {
                            for (int i = 0; i < pi.requestedPermissions.length; i++) {

                                String info = pi.requestedPermissions[i];

                                if (perms.contains(info)) {
                                    PermissionInfo pinfo = getContext().getPackageManager().getPermissionInfo(info, PackageManager.GET_META_DATA);
                                    if (permissionGroupInfo.name.equals(pinfo.group)) {
                                        BasicAppInfo basicAppInfo = new BasicAppInfo(applicationInfo,getContext().getPackageManager());

                                        if ((pi.requestedPermissionsFlags[i] & PackageInfo.REQUESTED_PERMISSION_GRANTED) != 0) {

                                            if(!group.appsGranted.contains(basicAppInfo)) {
                                                group.appsGranted.add(basicAppInfo);
                                            }
                                        }

                                        if(!group.allApps.contains(basicAppInfo)) {
                                            group.allApps.add(basicAppInfo);
                                        }

                                    }
                                }
                            }
                        }


                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                resultList.add(group);

                if(group.groupName.equals(Manifest.permission_group.MICROPHONE)){
                    this.microphone = group;
                } else if(group.groupName.equals(Manifest.permission_group.LOCATION)){
                    this.location = group;
                }
                else if(group.groupName.equals(Manifest.permission_group.CAMERA)){
                    this.camera = group;
                }
            }

        }
        writeToDb(microphone,location, camera);
        return resultList;
    }


    private void writeToDb(PermissionGroup microphone,PermissionGroup location, PermissionGroup camera){
        SharedPreferences sharedPref = getContext()
                .getSharedPreferences("com.example.stefan.mydigitalid.PREFERENCES_FILE", Context.MODE_PRIVATE);

        String uniqueID;
        float mic;
        float loc;
        float cam;

        mic = sharedPref.getFloat("mic",-1);
        loc = sharedPref.getFloat("loc",-1);
        cam = sharedPref.getFloat("cam",-1);
        uniqueID = sharedPref.getString("uniqueUserID", null);

        SharedPreferences.Editor editor = sharedPref.edit();

        mic = microphone.appsGranted.size();
        editor.putFloat("mic",mic);

        loc = location.appsGranted.size();
        editor.putFloat("loc",loc);

        cam = camera.appsGranted.size();
        editor.putFloat("cam",cam);

        if(uniqueID == null) {
            uniqueID = UUID.randomUUID().toString();
            editor.putString("uniqueUserID", uniqueID);
        }

        editor.apply();

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userNode = database
                .getReference("permissionsData")
                .child(uniqueID);
        

        userNode.child("microphone").child("allowed").setValue(microphone.appsGranted.size());
        userNode.child("microphone").child("not_allowed").setValue(microphone.allApps.size()-microphone.appsGranted.size());

        userNode.child("location").child("allowed").setValue(location.appsGranted.size());
        userNode.child("location").child("not_allowed").setValue(location.allApps.size()-location.appsGranted.size());

        userNode.child("camera").child("allowed").setValue(camera.appsGranted.size());
        userNode.child("camera").child("not_allowed").setValue(camera.allApps.size()-camera.appsGranted.size());
    }
}
