package com.example.stefan.mydigitalid.model;

import android.support.annotation.NonNull;

/**
 * Created by ID on 10-Feb-18.
 */

public class CallStatistic implements Comparable<CallStatistic>
{
    public String callerName;
    public int numberOfTimesCalled;
    public int timeOfPhoneCalls;
    public int numberOfPhoneCalls;
    public CallStatistic(String name, int number, int duration, int numberCalls)
    {
        callerName=name;
        numberOfTimesCalled=number;
        timeOfPhoneCalls=duration;
        numberOfPhoneCalls=numberCalls;
    }
    @Override
    public boolean equals(Object obj)
    {
        CallStatistic callStatistic =(CallStatistic) obj;
        if(obj!=null && callerName
                .equals(callStatistic.callerName))
            return true;
        return false;
    }

    @Override
    public int compareTo(@NonNull CallStatistic callStatistic) {
        if(callStatistic.numberOfTimesCalled<numberOfTimesCalled)
            return -1;
        else if(callStatistic.numberOfTimesCalled>numberOfTimesCalled)
            return 1;
        return 0;
    }
}
