package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.FilesContainerPagerAdapter;

/**
 * Created by kristijan on 2/21/18.
 */

public class FilesContainerFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files_container,container,false);

        TabLayout tabLayout = view.findViewById(R.id.tabs_files_container);
        ViewPager viewPager = view.findViewById(R.id.pager_files_container);
        FilesContainerPagerAdapter adapter = new FilesContainerPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
}
