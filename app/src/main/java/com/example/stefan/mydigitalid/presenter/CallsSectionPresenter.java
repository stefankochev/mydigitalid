package com.example.stefan.mydigitalid.presenter;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.example.stefan.mydigitalid.model.Call;
import com.example.stefan.mydigitalid.services.CallsIntentService;
import com.example.stefan.mydigitalid.view.CallsSectionFragment;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jovan on 05-Feb-18.
 */

public class CallsSectionPresenter implements Serializable
{
    protected CallsSectionFragment mView;

    public void attachView(CallsSectionFragment view)
    {
        mView=view;
    }

    public void onDestroy()
    {
        mView=null;
    }

    public void onRefresh()
    {
        if(mView!=null)
        {
            Intent intent=new Intent(mView.getContext(), CallsIntentService.class);
            mView.getContext().startService(intent);
        }
    }

}
