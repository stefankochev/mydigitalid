package com.example.stefan.mydigitalid.model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jovan on 27-Jan-18.
 */

public class Contact implements Serializable
{
    public String displayName;
    public List<String> phoneNumbers;
    public List<String> emails;
    public Bitmap photo;
    public Contact(String displayName)
    {
        this.displayName=displayName;
        phoneNumbers=new ArrayList<>();
        emails=new ArrayList<>();
        photo=null;
    }
}
