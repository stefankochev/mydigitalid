package com.example.stefan.mydigitalid.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.AppInfo;
import com.example.stefan.mydigitalid.model.BasicAppInfo;

import java.util.List;

/**
 * Created by Stefan on 2/9/2018.
 */

public class PermissionAppsAdapter extends RecyclerView.Adapter<PermissionAppsAdapter.AppViewHolder> {

    private List<BasicAppInfo> apps;
    private PackageManager pm;
    private Context context;

    public class AppViewHolder extends RecyclerView.ViewHolder{

        public ImageView icon;
        public TextView name;

        public AppViewHolder(View itemView) {
            super(itemView);
            this.icon = (ImageView)itemView.findViewById(R.id.permission_app_icon);
            this.name = (TextView)itemView.findViewById(R.id.permission_app_name);
        }
    }

    public PermissionAppsAdapter(List<BasicAppInfo> apps, PackageManager pm, Context context){
        this.apps = apps;
        this.pm = pm;
        this.context = context;
    }


    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.permission_app_row,parent,false);
        return new AppViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppViewHolder holder, int position) {
        BasicAppInfo info = apps.get(position);
        holder.name.setText(info.appName);
        try {
            Drawable drawable=pm.getResourcesForApplication(info.packageName).getDrawable(info.iconRes);
            holder.icon.setImageDrawable(drawable);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

}
