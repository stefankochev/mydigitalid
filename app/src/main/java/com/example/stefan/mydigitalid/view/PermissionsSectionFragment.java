package com.example.stefan.mydigitalid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.PermissionsAdapter;
import com.example.stefan.mydigitalid.model.PermissionGroup;
import com.example.stefan.mydigitalid.presenter.PermissionsPresenter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Stefan on 2/8/2018.
 */

public class PermissionsSectionFragment extends Fragment {

    private static final String ARG_PERMISSIONS_PRESENTER = "permissions.presenter";
    private PermissionsPresenter mPresenter;
    private List<PermissionGroup> groupList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PermissionsAdapter adapter;

    private ProgressBar progressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if(arguments != null) {
            mPresenter = (PermissionsPresenter) arguments.getSerializable(ARG_PERMISSIONS_PRESENTER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section_permissions,container,false);

        progressBar = (ProgressBar)view.findViewById(R.id.permissions_progress_bar);
        progressBar.setVisibility(View.GONE);

        recyclerView = (RecyclerView)view.findViewById(R.id.permissions_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new PermissionsAdapter(groupList,getActivity());
        recyclerView.setAdapter(adapter);


        if(mPresenter!=null) {
            mPresenter.attachView(this);
            mPresenter.onRefresh();
        }

        return view;
    }

    @Override
    public void onResume() {
        mPresenter.attachView(this);
        mPresenter.onRefresh();
        super.onResume();
    }

    @Override
    public void onPause() {
        mPresenter.onDestroy();
        super.onPause();
    }


    public void setGroupList(List<PermissionGroup> list){
        this.groupList.clear();
        this.groupList.addAll(list);
        adapter.notifyDataSetChanged();
    }


    public static PermissionsSectionFragment newInstance(PermissionsPresenter presenter){
        Bundle args = new Bundle();
        args.putSerializable(ARG_PERMISSIONS_PRESENTER, presenter);

        PermissionsSectionFragment fragment = new PermissionsSectionFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public void showProgressBar(){progressBar.setVisibility(View.VISIBLE);}
    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

}
