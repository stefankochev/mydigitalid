package com.example.stefan.mydigitalid.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.CallStatistic;

import java.util.List;

/**
 * Created by ID on 12-Feb-18.
 */

public class FavouriteContactsAdapter extends RecyclerView.Adapter<FavouriteContactsAdapter.StatiscticsViewHolder>
{
    public List<CallStatistic> mCallStatistics;
    public LayoutInflater mLayoutInflater;
    public FavouriteContactsAdapter(List<CallStatistic> callStatistics, LayoutInflater layoutInflater)
    {
        mCallStatistics=callStatistics;
        mLayoutInflater=layoutInflater;
    }

    @Override
    public StatiscticsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=mLayoutInflater.inflate(R.layout.favourites_row,parent,false);
        return new StatiscticsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatiscticsViewHolder holder, int position)
    {
        holder.mCallerName.setText(mCallStatistics.get(position).callerName);
        holder.mStatistic.setText(mCallStatistics.get(position).numberOfTimesCalled+"");
    }

    @Override
    public int getItemCount() {
        return mCallStatistics.size();
    }


    public class StatiscticsViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mCallerName;
        public TextView mStatistic;

        public StatiscticsViewHolder(View itemView)
        {
            super(itemView);
            mCallerName=itemView.findViewById(R.id.favourite_name);
            mStatistic=itemView.findViewById(R.id.numberOfTimesCalled);
        }
    }
}
