package com.example.stefan.mydigitalid.presenter;
import android.content.Intent;
import com.example.stefan.mydigitalid.services.ContactsIntentService;
import com.example.stefan.mydigitalid.model.Contact;
import com.example.stefan.mydigitalid.view.ContactsSectionFragment;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jovan on 23-Jan-18.
 */
public class ContactsSectionPresenter implements Serializable
{
    protected ContactsSectionFragment mView;
    public void attachView(ContactsSectionFragment view)
    {
        mView=view;
    }
    public void onDestroy()
    {
        mView=null;
    }
    public void onRefresh()
    {
        Intent intent=new Intent(mView.getContext(), ContactsIntentService.class);
        mView.getContext().startService(intent);
    }
}

