package com.example.stefan.mydigitalid.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.adapters.ContactsItemAdapter;
import com.example.stefan.mydigitalid.model.Contact;
import com.example.stefan.mydigitalid.presenter.ContactsSectionPresenter;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jovan on 23-Jan-18.
 */

public class ContactsSectionFragment extends Fragment
{
    private static final String ARG_CONTACTS_PRESENTER="com.example.stefan.mydigitalid.contacts.presenter";
    public static final String CONTACTS_ARRAY="com.example.stefan.mydigitalid.contacts.array";
    public static final String PICTURES_ARRAY="com.example.stefan.mydigitalid.pictures.array";
    public static final String CONTACTS_BROADCAST_ACTION="com.example.stefan.mydigitalid.contacts.broadcast.intent";
    private ContactsSectionPresenter mPresenter;
    public TextView mNumberOfContacts;
    public ProgressBar mProgressBar;
    private ContactsItemAdapter mContactsItemAdapter;
    private List<Contact> mContactsList;
    private BroadcastReceiver mContactsReceiver;
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle arguments=getArguments();
        if(arguments!=null)
        {
            mPresenter=(ContactsSectionPresenter)arguments.getSerializable(ARG_CONTACTS_PRESENTER);
        }
        mContactsList=new ArrayList<>();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.fragment_section_contacts,container,false);
        TouchScrollBar touchScrollBar =view.findViewById(R.id.touchScrollBar);
        touchScrollBar.setIndicator(new AlphabetIndicator(getActivity().getApplicationContext()),false);
        RecyclerView mRecyclerView=view.findViewById(R.id.contacts_recycler_view_id);
        mContactsItemAdapter=new ContactsItemAdapter(mContactsList,getActivity().getApplicationContext(),mRecyclerView);
        mRecyclerView.setAdapter(mContactsItemAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        mNumberOfContacts=view.findViewById(R.id.number_of_contacts);
        mProgressBar=view.findViewById(R.id.contacts_progress_bar);
        mProgressBar.setVisibility(View.INVISIBLE);
        swipeRefreshLayout=view.findViewById(R.id.contacts_swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                mPresenter.onRefresh();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mProgressBar.setVisibility(View.VISIBLE);
        mContactsReceiver=new ContactsReceiver();
        IntentFilter intentFilter=new IntentFilter(CONTACTS_BROADCAST_ACTION);
        getContext().registerReceiver(mContactsReceiver,intentFilter);
        if(mPresenter!=null)
        {
            mPresenter.attachView(this);
            mPresenter.onRefresh();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getContext().unregisterReceiver(mContactsReceiver);
        setContactsList(new ArrayList<Contact>());
        mPresenter.onDestroy();
    }
    public void setContactsList(List<Contact> contactsList)
    {
        mContactsList.clear();
        mContactsList.addAll(contactsList);
        mContactsItemAdapter.notifyDataSetChanged();
    }
    public static ContactsSectionFragment newInstance(ContactsSectionPresenter presenter)
    {
        //if(mPresenter==null)  ako e singleton pattern
        //TODO Singleton pattern
        Bundle args=new Bundle();
        args.putSerializable(ARG_CONTACTS_PRESENTER,presenter);
        ContactsSectionFragment contactsSectionFragment=new ContactsSectionFragment();
        contactsSectionFragment.setArguments(args);
        return contactsSectionFragment;
    }
    private class ContactsReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            ArrayList<Contact> contactsArray=(ArrayList<Contact>)intent.getSerializableExtra(CONTACTS_ARRAY);
            ArrayList<Bitmap> contactsImages=intent.getParcelableArrayListExtra(PICTURES_ARRAY);
            int i=0;
            for (Bitmap photo:contactsImages)
            {
                contactsArray.get(i).photo=photo;
                i++;
            }
            setContactsList(contactsArray);
            mNumberOfContacts.setText("Number of contacts: "+contactsArray.size());
            mProgressBar.setVisibility(View.INVISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
