package com.example.stefan.mydigitalid.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.stefan.mydigitalid.R;
import com.example.stefan.mydigitalid.model.Contact;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jovan on 24-Jan-18.
 */

public class ContactsItemAdapter extends RecyclerView.Adapter<ContactsItemAdapter.ContactsViewHolder> implements INameableAdapter
{
    private List<Contact> mContactsList;
    private LayoutInflater mLayoutInflater;
    private static final int UNSELECTED = -1;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;
    public ContactsItemAdapter(List<Contact> contactsList, Context context,RecyclerView recyclerView)
    {
        mContactsList=contactsList;
        mLayoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.recyclerView = recyclerView;
    }
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=mLayoutInflater.inflate(R.layout.contacts_row,parent,false);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position)
    {
        String contactNumbers="Numbers: ";
        String contactEmails="Emails: ";
        boolean isSelected = position == selectedItem;
        holder.expandButton.setSelected(isSelected);
        holder.mExpandableLayout.setExpanded(isSelected, false);
        holder.mContactName.setText(mContactsList.get(position).displayName);
        if(mContactsList.get(position).photo!=null)
        {
            Bitmap roundedBitmap=getRoundedBitmap(mContactsList.get(position).photo);
            holder.mContactImage.setImageBitmap(roundedBitmap);
        }
        else
            holder.mContactImage.setImageResource(R.mipmap.no_photo_img);
        if(mContactsList.get(position).phoneNumbers.size()==0)
            contactNumbers=contactNumbers+"//";
        else
        {
            for (String number:mContactsList.get(position).phoneNumbers)
            {
                contactNumbers=contactNumbers+number+"\n";
            }
        }
        holder.mContactNumbers.setText(contactNumbers);
        if(mContactsList.get(position).emails.size()==0)
            contactEmails=contactEmails+"\\";
        else
        {
            for (String email:mContactsList.get(position).emails)
            {
                contactEmails=contactEmails+email+"\n";
            }
        }
        holder.mContactEmails.setText(contactEmails);
        if(holder.mExpandableLayout.isExpanded())
        {
            holder.expandImage.setRotation(90f);
        }
    }

    @Override
    public int getItemCount() {
        return mContactsList.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        return Character.toUpperCase(mContactsList.get(element).displayName.charAt(0));
    }

    public class ContactsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener
    {
        private CircleImageView mContactImage;
        private TextView mContactName;
        private TextView mContactEmails;
        private ExpandableLayout mExpandableLayout;
        private TextView mContactNumbers;
        private ImageButton expandImage;
        private RelativeLayout expandButton;
        public ContactsViewHolder(View itemView)
        {
            super(itemView);
            mContactName=(TextView)itemView.findViewById(R.id.contact_name);
            mContactImage=(CircleImageView) itemView.findViewById(R.id.contact_image);
            mExpandableLayout=itemView.findViewById(R.id.contacts_expandable_layout);
            mExpandableLayout.setInterpolator(new OvershootInterpolator());
            mExpandableLayout.setOnExpansionUpdateListener(this);
            mContactNumbers=itemView.findViewById(R.id.contact_numbers);
            mContactEmails=itemView.findViewById(R.id.contact_emails);
            expandImage=itemView.findViewById(R.id.contact_expand_arrow);
            expandButton=itemView.findViewById(R.id.contacts_expand_button);
            expandButton.setOnClickListener(this);
        }
        @Override
        public void onClick(View view)
        {
            ContactsViewHolder holder = (ContactsViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
            if (holder != null) {
                holder.expandButton.setSelected(false);
                holder.mExpandableLayout.collapse();
                holder.expandImage.animate().rotation(0f);
                this.setIsRecyclable(true);
            }

            int position = getAdapterPosition();
            if (position == selectedItem) {
                selectedItem = UNSELECTED;
            } else {
                expandButton.setSelected(true);
                mExpandableLayout.expand();
                selectedItem = position;
                expandImage.animate().rotation(90f);
                this.setIsRecyclable(false);
            }
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state)
        {
            if (state == ExpandableLayout.State.EXPANDING) {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }
    }
    public Bitmap getRoundedBitmap(Bitmap bitmap){
        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        return circleBitmap;
    }
}
