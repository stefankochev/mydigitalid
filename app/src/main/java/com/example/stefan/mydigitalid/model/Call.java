package com.example.stefan.mydigitalid.model;

import java.io.Serializable;

/**
 * Created by Jovan on 06-Feb-18.
 */

public class Call implements Serializable
{
    public String number;
    public String callerName;
    public String type;
    public String duration;
    public String date;
}
