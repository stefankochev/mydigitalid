package com.example.stefan.mydigitalid.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Stefan on 2/17/2018.
 */

public class PermissionsApiClient {

    public static final String BASE_URL = "https://us-central1-mydigitalid-b0ce6.cloudfunctions.net/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
