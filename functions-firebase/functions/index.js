const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


exports.getPermissionsStatistics = functions.https.onRequest((req, res) => {
    var query = admin.database().ref("permissionsData");
    return query.once("value").then((snapshot) => {
        var sumCam = 0;
        var sumMic = 0;
        var sumLoc = 0;
        var numOfApps = 0;
        snapshot.forEach((child) => {
        	var num = child.child("camera/allowed").val();
        	sumCam += num;
        	num = child.child("microphone/allowed").val();
        	sumMic += num;
        	num = child.child("location/allowed").val();
        	sumLoc += num;
        	numOfApps++;
        })

        var result = {"camera":(sumCam/numOfApps),"location":(sumLoc/numOfApps),"microphone":(sumMic/numOfApps)};
        res.send(result);
        return snapshot.val();
    })
})

exports.getFilesStatistics = functions.https.onRequest((req, res) => {
    var query = admin.database().ref("filesData");
    return query.once("value").then((snapshot) => {
        var sumAudio = 0;
        var sumImage = 0;
        var sumVideo = 0;
        var sumText = 0;
        var numOfUsers = 0;
        snapshot.forEach((child) => {
        	var num = child.child("audio/totalSize").val();
        	sumAudio += num;
        	num = child.child("image/totalSize").val();
            sumImage += num;
            num = child.child("text/totalSize").val();
            sumText += num;
            num = child.child("video/totalSize").val();
            sumVideo += num;

        	numOfUsers++;
        })

        var result = {"audio":(sumAudio/numOfUsers)
                    ,"image":(sumImage/numOfUsers)
                    ,"text":(sumText/numOfUsers)
                    ,"video":(sumVideo/numOfUsers)};
        res.send(result);
        return snapshot.val();
    })
})